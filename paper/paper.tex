\documentclass{article} % For LaTeX2e
%\usepackage{nips15submit_e,times}
\usepackage{nips15submit_e,times}
\usepackage{url}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{color}
\usepackage{xcolor}
\usepackage{subfigure}
\usepackage{xspace}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{nicefrac}
\usepackage[ruled]{algorithm2e}
\usepackage{color}
\usepackage{xcolor}
\usepackage{mathrsfs}
\usepackage{tikz}
\usepackage{geometry}
\usetikzlibrary{arrows}

\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false,citecolor=red,backref=false]{hyperref}
\usepackage{amssymb,amsmath,amsthm, mathtools}
\usepackage[inline,draft]{fixme}

\newcommand{\nR}{\mathbb{R}}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\rank}{rank}



\title{WIP: Graph Dynamics}


\author{
Marc Niethammer\thanks{additional info} \\
UNC, Chapel Hill
\And
Roland Kwitt \\
University of Salzburg}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\nipsfinalcopy

\begin{document}
\maketitle

\begin{abstract}
This work addresses how to model evolutions of graphs.
TODO.
\end{abstract}

\section{Some thoughts on simple approaches to modeling}

Simple approaches to adress the modeling of graph dynamics that should be explored as baseline cases are
\begin{itemize}
  \item Euclidean statistics/modeling directly on the adjacency matrices. 
  \item Euclidean statistics/modeling on the graph Laplacian matrices.
  \item Euclidean statistics/modeling on the modified adjacency matrices, where $\overline{w_{ij}} = \log w_{ij}$. Then by construction we will end up with positive weithts by subsequent exponentiation.
  \item Euclidean statistics/modeling, but with explicit models for temporal change and local spatial change (as a real longitudinal mixed effects model). Here, we could for example postulate that edges which are close to each other should be strongly correlated.
  \item Euclidean statistics/modeling directly for features of interest (instead of extracting them by a modeled graph).
\end{itemize}

\section{Approaches Using the graph Laplacian}
\paragraph{Relationship between graph Laplacian and adjacency matrix.}

Let $G = (V,E)$ be an undirected graph with the vertex
set $V=\{v_1,\ldots,v_n\}$ and edge set $E$ with no self loops
or parallel edges. The edge set $E$ consists of pairs of 
integers $e_{ij} := (v_i,v_j)$ such that $1\leq i,j\leq n$ and $i\neq j$.
We further have a weight function $w$, satisfying $w_{ij} := w(e_{ij}) = w(e_{ji}) := w_{ji}$ and $w_{ij} \geq 0$, that
assigns a weight to each each in $E$.
The $n \times n$ adjacency matrix 
$\mathbf{A} = [a_{ij}]$ for the
undirected, edge-weighted graph $G$ is then defined as
\begin{equation}
a_{ij} := 
\begin{cases}
w_{ij} & \text{if } (i,j) \in E\\
0 & \text{otherwise}
\end{cases}
\end{equation}
The $n \times n$ degree matrix $\mathbf{D} 
= [d_{ij}]$ is defined as
\begin{equation}
d_{ij} := 
\begin{cases}
\sum_{j=1}^n w_{ij} & \text{if } i = j\\
0 & \text{otherwise}
\end{cases}
\end{equation}
The weighted graph Laplacian can then be expressed as 
$\mathbf{L} = \mathbf{D}-\mathbf{A}$ and factored\footnote{The operator $\diag$ when applied to a $n \times 1$ vector $\mathbf{a}$ returns a $n\times n$ diagonal matrix $\mathbf{A}$ with diagonal elements $a_i$; when applied
to a matrix $\textbf{A}$, it returns the $n\times 1$ vector $\mathbf{a}$, resp.} into $\mathbf{U}\diag(\mathbf{w})\mathbf{U}^\top$ where $\mathbf{U} = [u_{ij}]$ denotes the $n \times |E|$ (oriented) 
incidence matrix with elements
\begin{equation}
u_{ij} := 
\begin{cases}
+1 & \text{in case of $e_{ij}$}\\
-1 & \text{in case of $e_{ji}$}\\
0 & \text{otherwise}
\end{cases}
\label{eq:orientedincidence}
\end{equation}
and $w$ is a $|E|$-dimensional vector of edge weights. 
In other words, each column $\mathbf{u}_{*j}$ (corresponding to an edge $e_j$) 
of $\mathbf{U}$ has '$+1$' for 
one vertex of that edge and '$-1$' for the other. 
Obviously, this factorization is only 
unique up to a negation of columns.
Note that $L$ is symmetric and (weakly) diagonally dominant, i.e., $\forall i: l_{ii} \geq \sum_j |l_{ij}|$. Symmetry follows from the fact that $G$ is undirected and the weak diagonal dominance follows from the fact that all non-diagonal entries in $L$ are non-positive and the row-sum is $0$. Symmetric and weakly diagonally dominant matrices are also positive semi-definite. Further, the number of connected components in $G$ is given by the number of zero eigenvalues. So, for a graph with only one connected component we have that $\mathbf{L}$ is of rank 
$n-1$.

Now, given a graph Laplace matrix $\mathbf{L}$ of $G$, we can reconstruct the corresponding adjacency matrix $\mathbf{A}$ simply as 
\begin{equation}
\mathbf{A} = -(\mathbf{L} - \diag(\diag(\mathbf{L})))\enspace.
\end{equation}
Alternatively, we could factor $\mathbf{L}$ into $\mathbf{U}\diag(\mathbf{w})\mathbf{U}^\top$ 
and reconstruct $\mathbf{A}$ from the non-zero entries in 
$\mathbf{U}$ and 
the edge weights in $\mathbf{w}$.

\section{Laplacian dynamics via rank extension}

\paragraph{Notation.} We denote by $\mathcal{S}_+^n$ 
the space of $n\times n$ \textit{symmetric positive semidefinite (SPD)} matrices 
and by $\mathcal{S}_{++}^n$ the 
space of $n \times n$ \textit{symmetric positive definite (SPD)} matrices. For 
$\mathbf{A} \in \mathcal{S}_{+}^n$
we also write $A \succeq 0$, for $\mathbf{A} \in \mathcal{S}^n_{++}$ we 
write $\mathbf{A} \succ 0$. The space of symmetric matrices is 
simply denoted by $\mathcal{S}^n$. Further, $\mathbf{1}$ denotes the 
column vector of all ones.

In the following, we restate the characteristic properties
of the $n \times n$ graph Laplacian $\mathbf{L}$ with one connected
component from \cite{Ginestet14a}:
\begin{enumerate}[label=(\arabic*),font=\itshape]
\item $\mathbf{L} \in \mathcal{S}_+^n$
\item $\rank(\mathbf{L})=n-1$
\item $\mathbf{L}$ has row/column sum $0$
\item $\mathbf{L}$ has only negative off-diagonal entries, i.e., $l_{ij}<0$
\end{enumerate}

$\mathbf{L}$ has exactly one zero eigenvalue (since, the graph has one
connected component). By extending $\mathbf{L}$ to $\hat{\mathbf{L}}=
\mathbf{L}+\mathbf{1}\mathbf{1}^\top\frac{1}{m}$ we obtain a full-rank matrix 
$\hat{\mathbf{L}} \in \mathcal{S}_{++}^n$ with $\lambda_1=1$. The row/column
sum is $1$ and weak diagonal dominance is preserved. However, those SPD
matrices that arise from Laplacians by a rank-1 extension no longer form
a convex cone, since, e.g., $\alpha\hat{\mathbf{L}}$ for $\alpha \in \mathbb{R}_+$ 
violates the row/column sum constraint and $\lambda_1 \neq 1$ anymore. 

\begin{lemma}
The set $\mathcal{R} \subset \mathcal{S}_{++}^n$ of SPD matrices obtained
by extending $\mathbf{A} \in \mathcal{L}$ by $\mathbf{1}\mathbf{1}^\top
\nicefrac{1}{m}$ with row/column sum of $0$ and $\lambda_1=1$ forms
a convex set.
\end{lemma}
\begin{proof}
It is easy to see that $(1-\alpha)\mathbf{A} + \alpha\mathbf{B} \in \mathcal{R}$
for $\mathbf{A},\mathbf{B} \in \mathcal{R}$ and $\alpha \in (0,1)$.
\end{proof}

The question is if we can leverage the well-studied algebraic and differential
structure of SPD matrices when studying the evolution of Laplacians over time.
This is only possible if, e.g., moving along geodesic curves in $\mathcal{S}_{++}^n$
preserves the structure of the underlying Laplacian. In other words, operations
on elements of $\mathcal{R}$, based on the differential structure of $\mathcal{S}_{++}^n$ 
should remain in $\mathcal{R}$. If this is the case, we can simply ``go back'' to 
the underlying graph Laplacian via 
$\mathbf{L}=\hat{\mathbf{L}}-\mathbf{1}\mathbf{1}^\top\nicefrac{1}{m}$.

In particular, we will
look at the differential structure of $\mathcal{S}_{++}^n$ under the log-Euclidean 
metric of \cite{Arsigny07a}, primarily due its computational advantages and 
closed-form expressions for the Riemannian exponential and logarithmic maps, 
as well as geodesics.

\paragraph{Log-Euclidean space.} TODO

Experimentally, all the following points are fine! However, we do need proofs
or, at least, a straightforward reasoning.

\begin{itemize}
\item Show that, along geodesics, $\lambda_1=1$ is preserved (easy)
\item Symmetry is preserved by construction, since SPD
\item The matrix logarithm from $\mathcal{S}_{++}^n$ to $\mathcal{S}^n$ is a diffeomorphism \cite{Arsigny07a}
\item Show that row/column sum is preserved to be 1
\item Show that result is weakly-diagonally dominant
\item If last point holds, all off-diagonals need to be negative  
\item $\mathcal{S}^n$ is a vector space
\item The matrix logarithm in our case even maps to PSD matrices (not only symmetric), since we deal with diagonally dominant matrices (which in together with symmetric is a sufficient condition for PSD)
\item Does taking the matrix exponential back from PSD matrices to SPD matrices hit only elements in $\mathcal{R}$ (maybe this is obvious, but I don't see it) 
\item Does ridge regression really work in log-Euclidean space, since we are no longer in a convex cone?
\end{itemize}




\section{Related work}
\label{section:related_work}

\cite{Boumal11a}
\cite{Arsigny07a}
\cite{Ginestet14a}
\cite{Moakher11a}
\cite{Absil08a} % Absil book
\cite{Vandereycken13a}, % SPD-fix-rank geometry
\cite{Kolar11a} % Reconstruction of connectivity from node 
\cite{Richard10a,Richard14a} % Link prediction


\section{Projections}
\label{sec:projections}

\subsection{Projection of a symmetric positive semidefinite matrix to one with zero row-sums}

\subsubsection{Projection based on the factorization of both matrices}
\label{sec:projection_factored}

We are given a matrix $S$ which is asummed to symmetric and positive semidefinite (sPSD), but which not necessarily has zero row-sums 
as is the case for graph Laplacian matrices. We assume that this matrix can be factored as $S = Q Q^T$ with $Q\in\mathbb{R}^{n\times p}$.
For simplicity start by finding the closest matrix $Y$ to $Q$ in the Frobenius norm sense such that $Y Y^T \bm{1}=\bm{0}$, where $\bm{1}\in\mathbb{R}^{n\times 1}$ is a vector of all ones, $Y\in\mathbb{R}^{n\times p}$ and $YY^T$ is meant to approximate $S$.

The optimization problem is then to minimize
\begin{equation}
  E(Y) = \|Y-Q\|_F^2\quad \text{such that}\quad Y Y^T\bm{1} = \bm{0}.\label{eq:projection_factored}
\end{equation}
A key observation is that since $Y Y^T\bm{1} = \bm{0}$ it follows that $Y^T Y Y^T\bm{1} = \bm{0}$.
But since $Y^T Y$ is invertible this implies $Y^T\bm{1}=\bm{0}$. Hence, the minimization problem can be simplified to

\begin{equation}
  E(Y) = \|Y-Q\|_F^2\quad \text{such that}\quad Y^T\bm{1} = \bm{0}.
\end{equation}

Adding the equality contraint with a Lagrangian multiplier vector, $\bm{\lambda}$, we obtain the saddle-point problem
\begin{equation}
  E(Y,\bm{\lambda}) = \bm{\lambda}^TY^T\bm{1} + \Tr (Y-Q)^T(Y-Q). 
\end{equation}
Computing the Karush-Kuhn-Tucker (KKT) conditions (ie., computing the derivatives with respect to $Y$ and $\bm{\lambda}$ and setting them to zero) results in
\begin{eqnarray}
  \frac{\partial E(Y,\bm{\lambda})}{\partial Y} &=& 2Y - 2Q + \bm{1}\bm{\lambda}^T = 0,\label{eq:KKTY} \\
  \frac{\partial E(Y,\bm{\lambda})}{\partial \bm{\lambda}} &=& Y^T\bm{1} = \bm{0}.\label{eq:KKTl}
\end{eqnarray}
From Equation~\ref{eq:KKTY} we obtain
\begin{equation}
  Y = Q - \frac{1}{2}\bm{1}\bm{\lambda}^T.\label{eq:Y_LP}
\end{equation}
Substituting this result into Equation~\ref{eq:KKTl} results in
\begin{equation}
  \bm{0} = (Q-\frac{1}{2}\bm{1}\bm{\lambda}^T)^T\bm{1} = Q^T\bm{1} - \frac{1}{2} \bm{\lambda}\bm{1}^T\bm{1} = Q^T\bm{1} - \frac{n}{2} \bm{\lambda}.
\end{equation}
Therefore
\begin{equation}
  \bm{\lambda} = \frac{2}{n} Q^T\bm{1}.
\end{equation}
Substituting this expression for $\bm{\lambda}$ into Equation~\ref{eq:Y_LP} results in
\begin{equation}
Y = Q-\frac{1}{n}\bm{1}\bm{1}^TQ = (I-\frac{1}{n}E)Q,
\end{equation}
where $E\in\mathbb{R}^{n\times n} = \bm{1} \bm{1}^T$ is a matrix of all ones.

\subsubsection{Projection in the non-factorized space}
\label{sec:projection_non_factored}

One could argue that the minimization problem of Equation~\ref{eq:projection_factored} is not exactly what we want, because we care about approximation of the actual matrices and not of the factors $Q$ and $Y$. It turns out that the obtained solution is also optimal for the non-factored space, i.e., that it is a minimizer for
\begin{equation}
  E(Y) = \|Y Y^T - S\|_F^2 \quad \text{such that}\quad Y Y^T\bm{1} = \bm{0}.\label{eq:projection_non_factored}.
\end{equation}
As before this the constraint can be replaced by $Y^T\bm{1}=\bm{0}$. The saddle-point problem is then
\begin{equation}
   E(Y,\bm{\lambda}) = \bm{\lambda}^TY^T\bm{1} + \Tr (YY^T-S)^T(YY^T-S). 
\end{equation}
The KKT conditions are
\begin{eqnarray}
  \frac{\partial E(Y,\bm{\lambda})}{Y} &=& -2SY - 2S^T Y + 4 YY^TY+\bm{1}\bm{\lambda}^T = -4SY + 4YY^TY + \bm{1}\bm{\lambda}^T = 0,\label{eq:KKT_Y_nf}\\
  \frac{\partial E(Y,\bm{\lambda})}{\bm{\lambda}} &=& \bm{1}^T Y = \bm{0}.\label{eq:KKT_l_nf}
\end{eqnarray}
Multiplying Equation~\ref{eq:KKT_Y_nf} from the left by $\bm{1}^T$ and noting that $\bm{1}^T\bm{1}=n$ we obtain 
\begin{equation}
  \bm{\lambda} = \frac{4}{n}\left(Y^TS-Y^TYY^T\right)\bm{1} = \frac{4}{n}Y^T S \bm{1},
\end{equation}
where the second equality followed from Equation~\ref{eq:KKT_l_nf}. Substituting $\lambda$ into Equation~\ref{eq:KKT_Y_nf} we obtain
\begin{equation}
  -SY + YY^TY + \frac{1}{n}\bm{1}\bm{1}^T S Y = 0.\label{eq:opt_Y_nf}
\end{equation}
Our claim was that $Y=(I-\frac{1}{n}E)Q$ also fulfills these optimality conditions. Clearly,
\begin{equation}
  Y^T\bm{1} = Q^T(I-\frac{1}{n}\bm{1}\bm{1}^T)\bm{1} = Q^T(\bm{1}-\bm{1}) = \bm{0}.
\end{equation}
Substituting $Y=(I-\frac{1}{n}E)Q$ into Equation~\ref{eq:opt_Y_nf} we obtain after some algebra
\begin{equation}
  (\frac{1}{n}E-I)QQ^T(I-\frac{1}{n}E)Q + (I-\frac{1}{n}E)QQ^T(I-\frac{1}{n}E)(I-\frac{1}{n}E)Q = 0.
\end{equation}
Since $(I-\frac{1}{n}E)(I-\frac{1}{n}E)= (I-\frac{1}{n}E)$ this is true for all Q. Hence, $Y=(I-\frac{1}{n})Q$ is also the optimal solution in the non-factored space.

\subsection{Best initial condition assuming a straight line geodesic for $S_+(n,p)$}

In~\cite{Vandereycken13a} a parameterization of the tangent space for the space of symmetric postitive semidefinite matrices of fixed rank, $S_+(n,p)$ is proposed and the related geodesic equations based on the Euclidean metric are derived. The authors of~\cite{Vandereycken13a} point out that under certain conditions the geodesic between two sPSD matrices becomes a straight line (ie., can be obtained by linear interpolation between the matrices). Therefore it is naturally to ask for two arbitrary matrices $S_0,S_1\in S_+(n,p)$ what the best initial condition for a geodesic, starting at $S_0$ and approximately arriving at $S_1$ in unit time, would be if we restrict ourselves to such straight line solutions. The general geodesic is parameterized through $H=H^T\in\mathbb{R}^{p\times p}$, $Z\in\mathbb{R}^{n\times p}$, and $Y\in\mathbb{R}^{n\times p}$. However, according to~\cite{Vandereycken13a} we know that for a straight line solution $Z(t)=0$ and $S(t)=Y_0(I+2tH_0)Y_0^T$. Hence, we seek to minimize
\begin{equation}
E(H_0) = \|S_1-Y_0(I+2H_0)Y_0^T\|_F^2
\end{equation}
in the Frobenius norm sense. We know that $H$ has to be symmetric, hence we minimize instead
\begin{equation}
  E(V) = \|S_1-Y_0(I + V + V^T )Y_0^T\|_F^2,
\end{equation}
where $H = \frac{V+V^T}{2}$ and hence symmetric by construction. This equation may be rewitten as
\begin{equation}
  E(V) = \Tr (S_1-Y_0(I+V+V^T)Y_0^T)^T(S_1-Y_0(I+V+V^T)Y_0^T).
\end{equation}
After some algebra and calculus we obtain
\begin{equation}
  \frac{\partial E}{\partial V} = -2 Y_0^T( S_1^T + S_1 )Y_0 + 4 Y_0^TY_0 Y_0^TY_0 + 4 Y_0^TY_0( V + V^T ) Y_0^TY_0,
\end{equation}
which needs to vanish at optimality. As we can invert $Y_0^TY_0$ we obtain
\begin{equation}
  H_0 = \frac{V^T + V}{2} = \frac{1}{2}\left[(Y_0^TY_0)^{-1}Y_0^T\left(\frac{S_1^T + S_1}{2}\right)Y_0(Y_0^TY_0)^{-1}-I\right].
\end{equation}
Hence, given a factorization of the initial sPSD matrix $S_0=Y_0 Y_0^T$ the optimal initial conditions for a straight line solution are
$(Y,H,Z)=(Y_0,H_0,0)$.

\section{Geodesic Equations}
\label{sec:geodesic_equations}

\subsection{Geodesic Equations Based on the Metric Induced by the Euclidean Inner Product}

Our goal is to parameterize evolutions of graph Laplacian matrices. These are symmetric matrices with zero row sum (zero column sum respectively, due to symmetry). Laplacian matrices, $L\in\mathbb{R}^{n\times n}$ have rank $n-1$, are symmetric, and positive semi-definite. The null-space is spanned by the vector of all ones. Off-diagonal elements are negative. Ignoring the sign-constraints for the off-diagonal elements we can parameterize such matrices as $L=YY^T$ where $L\in\mathbb{R}^{n\times n-1}$. Enforcing the zero row-sum condition requires $YY^T\bm{1}=\bm{0}$ or alternatively (as we have seen in Section~\ref{sec:projections}) $Y^T\bm{1}=\bm{0}$.\fixme{This may all be oversimplified. Not sure. It is possible I should look at the tangent space for the fixed rank case and combine the two. Will look at this next.}

Hence, in the $Y$ parameterization we can characterize the tangent space by requiring the directional derivative of $F(Y)=Y^T\bm{1}$ to be zero. Formally, we compute
\begin{equation}
  DF\arrowvert_Y(\psi) = \frac{\partial F(Y+\epsilon\psi)}{\partial \epsilon}\arrowvert_{\epsilon=0} = \psi^T\bm{1}= 0.
\end{equation}
We can then characterize the tangent space as
\begin{equation}
  T_YS = \{\Psi:\Psi^T\bm{1}=0,~\Psi\in\mathbb{R}^{n\times n-1}\}.
\end{equation}
This also immediately suggests an over-parameterization of a tangent vector through $Q\in\mathbb{R}^{n\times n-1}$ by minimizing
\begin{equation}
  E(\Psi)=\|\Psi-Q\|_F^2\quad\text{such that}\quad \Psi^T\bm{1}=\bm{0}.
\end{equation}
This is of course the same problem as the projection of Section~\ref{sec:projection_factored}. Therefore, we can parameterize a tangent vector, through $Q$, as
\begin{equation}
  \Psi = (I-\frac{1}{n}E)Q.
\end{equation}

Now that we have a parameterization we can compute the geodesic equations by computing the variation of
\begin{equation}
  E(Y,Q,\lambda) = \int \frac{1}{2}\Tr \dot{Y}^T\dot{Y} + \Tr\lambda^T(\dot{Y}-PQ)~dt,
\end{equation}
where $P:=(I-\frac{1}{n}E)$. For the sake of deriving the geodesic equation we ignore boundary conditions for notational simplicity. Computing the variation we obtain
\begin{multline}
  \delta E(Y,Q,\lambda;\delta Y, \delta Q, \delta \lambda ) = \int \frac{1}{2}\Tr (\dot{\delta Y}^T\dot{Y} + \dot{Y}^T\dot{\delta Y}) \\ + \Tr \delta\lambda^T(\dot{Y}-PQ) + \Tr \lambda^T(\dot{\delta Y}-P\delta Q)~dt.
\end{multline}
Integration by parts and collecting terms results in
\begin{equation}
  \delta E = \int \Tr \delta Y^T( -\ddot{Y}-\dot{\lambda} ) - \Tr \delta Q^TP\lambda + \Tr \delta\lambda^T(\dot{Y}-PQ)~dt.
\end{equation}
Therefore the geodesic equations are specified by the system
\begin{equation}
  \begin{cases}
    \ddot{Y}+\dot{\lambda}&=0,\\
    P\lambda &=0,\\
    \dot{Y}-PQ&=0.
  \end{cases}
\end{equation}
From the second equation we can conclude that $\lambda=E$, therefore $\dot{\lambda}=0$, and consequentially $\ddot{Y}=0$. Hence, $Y$ follows a straight line. Differentiating the third equation we obtain
\begin{equation}
  \ddot{Y} = P\dot{Q}.
\end{equation}
As this quantity needs to vanish we can conclude that the tangent space part of $Q$ is constant. Hence, the geodesic has (as expected) constant velocity. This geodesic equation is also in line with the observation for the fixed-rank evolutions of Laplace matrices which result in $Z=0$.

\section{Some Synthetic Data Results}

We are given the following two graphs $G_1 = (V_1,E_1)$ and $G_2 =(V_2,E_2)$ with one connected component each; hence
$\rank(L(G_i))=3$. We set $G(0) = G_1$ and $G(1) = G_2$.
\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=2.5cm,
  thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
  \node[main node] (1) {1};
  \node[main node] (2) [below left  of=1] {2};
  \node[main node] (3) [below right of=2] {3};
  \node[main node] (4) [below right of=1] {4};
  \path[every node/.style={font=\sffamily\small}]
	(1) edge node [right] {1} (2)
	(2) edge node [above] {2} (4)
	(4) edge node [right] {3} (3);
\end{tikzpicture}
\qquad\qquad
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=2.5cm,
  thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
  \node[main node] (1) {1};
  \node[main node] (2) [below left  of=1] {2};
  \node[main node] (3) [below right of=2] {3};
  \node[main node] (4) [below right of=1] {4};
  \path[every node/.style={font=\sffamily\small}]
	(1) edge node [right] {\textcolor{red}{9}} (2)
	(2) edge node [above] {2} (4)
	(4) edge node [right] {\textcolor{red}{4}} (3);
\end{tikzpicture}
\end{center}
\caption{Source $G(0)$ (left); Target $G(1)$ (right)}
\end{figure}

%\begin{equation}
%A(G_1) = \left[
%\begin{matrix}
%0 & 1 & 0 & 0\\
%1 & 0 & 0 & 2\\
%0 & 0 & 0 & 3\\
%0 & 2 & 3 & 0\\
%\end{matrix}
%\right]
%\qquad
%A(G_2) = \left[
%\begin{matrix}
%0 & 9 & 0 & 0\\
%9 & 0 & 0 & 2\\
%0 & 0 & 0 & 4\\
%0 & 2 & 4 & 0\\
%\end{matrix}
%\right]
%\label{eqn:adj_matrices}
%\end{equation}
%
%\begin{equation}
%D(G_1) = \left[
%\begin{matrix}
%1 & 0 & 0 & 0\\
%0 & 3 & 0 & 0\\
%0 & 0 & 3 & 0\\
%0 & 0 & 0 & 5\\
%\end{matrix}
%\right]
%\qquad
%D(G_2) = \left[
%\begin{matrix}
%9 & 0 & 0 & 0\\
%0 & 11 & 0 & 0\\
%0 & 0 & 4 & 0\\
%0 & 0 & 0 & 6\\
%\end{matrix}
%\right]
%\label{eqn:deg_matrices}
%\end{equation}

\noindent
Here are the corresponding Laplace matrices:
\begin{equation}
\begin{split}
L(G_1) =\left[
\begin{matrix}
  1    &-1    & 0    & 0\\
 -1    & 3    & 0    &-2\\
  0    & 0    & 3    &-3\\
  0    &-2    &-3    & 5\\
\end{matrix}
\right]\\
L(G_2) = \left[
\begin{matrix}
  9 &   -9  &   0  &   0\\
 -9 &   11  &   0  &  -2\\
  0 &    0  &   4  &  -4\\
  0 &   -2  &  -4  &   6\\
\end{matrix}
\right]
\end{split}
\end{equation}
... and the evolution of the graph at different times:
\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=2.5cm,
  thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
  \node[main node] (1) {1};
  \node[main node] (2) [below left  of=1] {2};
  \node[main node] (3) [below right of=2] {3};
  \node[main node] (4) [below right of=1] {4};
  \path[every node/.style={font=\sffamily\small}]
	(1) edge node [right] {1.9} (2)
	(2) edge node [above] {2} (4)
	(4) edge node [right] {3.3} (3);
\end{tikzpicture}
\hfill
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=2.5cm,
  thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
  \node[main node] (1) {1};
  \node[main node] (2) [below left  of=1] {2};
  \node[main node] (3) [below right of=2] {3};
  \node[main node] (4) [below right of=1] {4};
  \path[every node/.style={font=\sffamily\small}]
	(1) edge node [right] {5.8} (2)
	(2) edge node [above] {2} (4)
	(4) edge node [right] {3.8} (3);
\end{tikzpicture}
\hfill
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=2.5cm,
  thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
  \node[main node] (1) {1};
  \node[main node] (2) [below left  of=1] {2};
  \node[main node] (3) [below right of=2] {3};
  \node[main node] (4) [below right of=1] {4};
  \path[every node/.style={font=\sffamily\small}]
	(1) edge node [right] {7.2} (2)
	(2) edge node [above] {2} (4)
	(4) edge node [right] {3.9} (3);
\end{tikzpicture}
\end{center}
\caption{$G(t)$ at $t=0.3$, $t=0.8$ and $t=0.9$}
\end{figure}

\noindent
Let's extrapolate a little to $t=1.2$:

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=2.5cm,
  thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
  \node[main node] (1) {1};
  \node[main node] (2) [below left  of=1] {2};
  \node[main node] (3) [below right of=2] {3};
  \node[main node] (4) [below right of=1] {4};
  \path[every node/.style={font=\sffamily\small}]
	(1) edge node [right] {14} (2)
	(2) edge node [above] {2} (4)
	(4) edge node [right] {4.2} (3);
\end{tikzpicture}
\end{center}
\caption{$G(t)$ at $t=1.2$}
\end{figure}

\begin{figure}[h!]
\begin{tabular}{ccccc}
  \begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=1.5cm,
      thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
    \node[main node] (1) {1};
    \node[main node] (2) [below left  of=1] {2};
    \node[main node] (3) [below right of=2] {3};
    \node[main node] (4) [below right of=1] {4};
    \path[every node/.style={font=\sffamily\small}]
    (1) edge node [right] {$4$} (2)
    (2) edge node [above] {$5$} (4)
    (4) edge node [right] {$\frac{1}{2}$} (3);
  \end{tikzpicture}
  &
    \begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=1.5cm,
      thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
    \node[main node] (1) {1};
    \node[main node] (2) [below left  of=1] {2};
    \node[main node] (3) [below right of=2] {3};
    \node[main node] (4) [below right of=1] {4};
    \path[every node/.style={font=\sffamily\small}]
    (1) edge node [right] {$2$} (2)
    (2) edge node [above] {$5$} (4)
    (4) edge node [right] {$1$} (3);
  \end{tikzpicture}
    &
      \begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=1.5cm,
      thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
    \node[main node] (1) {1};
    \node[main node] (2) [below left  of=1] {2};
    \node[main node] (3) [below right of=2] {3};
    \node[main node] (4) [below right of=1] {4};
    \path[every node/.style={font=\sffamily\small}]
    (1) edge node [right] {$\sqrt{2}$} (2)
    (2) edge node [above] {$5$} (4)
    (4) edge node [right] {$\sqrt{2}$} (3);
  \end{tikzpicture}
      &
        \begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=1.5cm,
      thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
    \node[main node] (1) {1};
    \node[main node] (2) [below left  of=1] {2};
    \node[main node] (3) [below right of=2] {3};
    \node[main node] (4) [below right of=1] {4};
    \path[every node/.style={font=\sffamily\small}]
    (1) edge node [right] {$1$} (2)
    (2) edge node [above] {$5$} (4)
    (4) edge node [right] {$2$} (3);
  \end{tikzpicture}
        &
          \begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=1.5cm,
      thick,main node/.style={circle,fill=blue!20,draw,font=\sffamily\bfseries}]
    \node[main node] (1) {1};
    \node[main node] (2) [below left  of=1] {2};
    \node[main node] (3) [below right of=2] {3};
    \node[main node] (4) [below right of=1] {4};
    \path[every node/.style={font=\sffamily\small}]
    (1) edge node [right] {$\frac{1}{2}$} (2)
    (2) edge node [above] {$5$} (4)
    (4) edge node [right] {$4$} (3);
          \end{tikzpicture} \\ \hline
          $t=-1$ & $t=0$ & $t=\frac{1}{2}$ & $t=1$ & $t=2$
\end{tabular}
\end{figure}

\bibliographystyle{plain}
\bibliography{references}

\end{document}
