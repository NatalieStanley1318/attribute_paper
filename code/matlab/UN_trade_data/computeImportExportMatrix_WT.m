function A = computeImportExportMatrix_WT( ds, cc, cc_ids )

% first create a mapping from country code to country code id

mapObj = containers.Map( cc, cc_ids );

A = zeros( length( cc ) );

for i=1:length( ds )
  
  if ( mod( i, 10000 )==0 )
    fprintf('#');
  end
  
  icode = str2num( ds(i).icode );
  ecode = str2num( ds(i).ecode );
  value = ds(i).value;
  
  if ( mapObj.isKey( icode ) & mapObj.isKey( ecode ) )

    icode_id = mapObj( icode );
    ecode_id = mapObj( ecode );
  
    A( icode_id, ecode_id ) = A( icode_id, ecode_id ) + value;
    
  end
  
end

fprintf('\n');