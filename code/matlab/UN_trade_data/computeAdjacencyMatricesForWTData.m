clear all; close all;

dataDir = '/playpen/dataNotBackedUp/wtf/mat';
years = 1962:2000;

nrOfYears = length( years );
files = [];
for i=1:nrOfYears
  cYear = years( i );
  sYear = num2str( cYear );
  sYearShort = sYear(3:4);
  files{end+1} = strcat( 'wtf', sYearShort, '.mat' );
end

% get all the possible country codes
[cc,cn] = parseCountryCodes( 'countryCodes.txt' );
cc_ids = 1:length( cc );

% read them
data = [];

for i=1:nrOfYears
  currentFile = strcat( dataDir, '/', files{i} );
  fprintf( 'Processing %s\n', currentFile );
  load( currentFile );
  ds = dataset2struct( st_dataset );
  IE = computeImportExportMatrix_WT( ds, cc, cc_ids );
  data(end+1).IE = IE;
  data(end).A = IE+IE';
  data(end).year = years( i );
  clear ds;
  clear st_dataset;
end

save worldTrade_data cc cn cc_ids data