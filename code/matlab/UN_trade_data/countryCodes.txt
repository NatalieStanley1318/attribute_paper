0 00 000 World 100000 World 1962-2061 World
710 11 710 South Africa 117100 South Africa 2000-2061 South Africa
710 11 711 S.Afr.Cus.Un 117100 South Africa 1962-1999 South African Customs Union
12 13 012 Algeria 130120 Algeria 1962-2061 Algeria
12 13 290 Africa N.NES 138960 North Africa NES 1962-2061 North Africa Not Elsewhere Specified
434 13 434 Libya 134340 Libya 1962-2061 Libyan Arab Jamahiriya
504 13 504 Morocco 135040 Morocco 1962-2061 Morocco
732 13 732 Westn.Sahara 135040 Morocco 1962-2061 Western Sahara
736 13 736 Sudan 137360 Sudan 1962-2061 Sudan
788 13 788 Tunisia 137880 Tunisia 1962-2061 Tunisia
818 13 818 Egypt 138180 Egypt 1962-2061 Egypt
120 14 120 Cameroon 141200 Cameroon 1962-2061 Cameroon
140 14 140 Cent.Afr.Rep 141400 Cent.Afr.Rep 1962-2061 Central African Republic
148 14 148 Chad 141480 Chad 1962-2061 Chad
178 14 178 Congo 141780 Congo 1962-2061 Congo
226 14 226 Eq.Guinea 162260 Eq.Guinea 1962-2061 Equatorial Guinea
266 14 266 Gabon 142660 Gabon 1962-2061 Gabon
266 14 472 Afr.CACEU NS 148960 Afr.CACEU NS 1962-2061 Africa CACEU Not Elsewhere Specified
24 16 024 Angola 160240 Angola 1962-2061 Angola
72 16 072 Botswana 117100 South Africa 2000-2061 Botswana
86 16 086 Br.Ind.Oc.Tr 166900 Seychelles 1962-2061 British Indian Ocean Territories
108 16 108 Burundi 161080 Burundi 1962-2061 Burundi
132 16 132 Cape Verde 166240 GuineaBissau 1962-2061 Cape Verde
174 16 174 Comoros 166380 Fr Ind O 1962-2061 Comoros
180 16 180 Dem.Rp.Congo 161800 Dem.Rp.Congo 1962-2061 Congo Dem. Republic
204 16 204 Benin 162040 Benin 1962-2061 Benin
230 16 230 Fmr Ethiopia 162300 Ethiopia 1962-1992 Frm Ethiopia (until 1992)
230 16 231 Ethiopia 162300 Ethiopia 1993-2061 Ethiopia
230 16 232 Eritrea 162300 Ethiopia 1993-2061 Eritrea
260 16 260 Fr.So.Ant.Tr 166380 Fr Ind O 1962-2061 French South Antartic Territories
262 16 262 Djibouti 162620 Djibouti 1962-2061 Djibouti
270 16 270 Gambia 162700 Gambia 1962-2061 Gambia
288 16 288 Ghana 162880 Ghana 1962-2061 Ghana
324 16 324 Guinea 163240 Guinea 1962-2061 Guinea
384 16 384 Cote Divoire 163840 Cote Divoire 1962-2061 Cote Divoire
404 16 404 Kenya 164040 Kenya 1962-2061 Kenya
426 16 426 Lesotho 117100 South Africa 2000-2061 Lesotho
430 16 430 Liberia 164300 Liberia 1962-2061 Liberia
450 16 450 Madagascar 164500 Madagascar 1962-2061 Madagascar
454 16 454 Malawi 164540 Malawi 1965-2061 Malawi
466 16 466 Mali 164660 Mali 1962-2061 Mali
478 16 478 Mauritania 164780 Mauritania 1962-2061 Mauritania
480 16 480 Mauritius 164800 Mauritius 1962-2061 Mauritius
508 16 508 Mozambique 165080 Mozambique 1962-2061 Mozambique
516 16 516 Namibia 117100 South Africa 1962-2061 Namibia
562 16 562 Niger 165620 Niger 1962-2061 Niger
566 16 566 Nigeria 165660 Nigeria 1962-2061 Nigeria
566 16 577 Afr.Other NS 168960 Afr.Other NS 1962-2061 Other Africa Not Elsewhere Specified
624 16 624 GuineaBissau 166240 GuineaBissau 1962-2061 Guinea Bissau
638 16 638 Reunion 166380 Fr Ind O 1962-1995 Reunion
646 16 646 Rwanda 166460 Rwanda 1962-2061 Rwanda
654 16 654 St.Helena 166540 St.Helena 1962-2061 Saint Helena
678 16 678 Sao Tome Prn 166240 GuineaBissau 1962-2061 Sao Tome and Principe
686 16 686 Senegal 166860 Senegal 1962-2061 Senegal
690 16 690 Seychelles 166900 Seychelles 1962-2061 Seychelles
694 16 694 Sierra Leone 166940 Sierra Leone 1962-2061 Sierra Leone
706 16 706 Somalia 167060 Somalia 1962-2061 Somalia
716 16 716 Zimbabwe 167160 Zimbabwe 1965-2061 Zimbabwe
716 16 717 Fm Rhod Nyas 167170 Fm Rhod Nyas 1962-1964 Frm Rhodesia NYAS (until 1964)
748 16 748 Swaziland 117100 South Africa 2000-2061 Swaziland
768 16 768 Togo 167680 Togo 1962-2061 Togo
800 16 800 Uganda 168000 Uganda 1962-2061 Uganda
834 16 834 Tanzania 168340 Tanzania 1965-2061 United Republic of Tanzania
834 16 835 Fm Tanganyik 168350 Fm Tanganyik 1962-1964 Frm Tangayika (until 1964)
834 16 836 Fm Zanz-Pemb 168360 Fm Zanz-Pemb 1962-1964 Frm Zanzibar-Pemb. (until 1964)
854 16 854 Burkina Faso 168540 Burkina Faso 1962-2061 Burkina Faso
894 16 894 Zambia 168940 Zambia 1965-2061 Zambia
124 21 124 Canada 211240 Canada 1962-2061 Canada
630 21 841 USA, P.Rico 218400 USA 1962-1980 Frm USA ex.US Virgin Islds (until 80)
840 21 842 USA,PR,USVI 218400 USA 1981-2061 USA, incl. Puerto Rico and US Virgin Islands
60 22 060 Bermuda 220600 Bermuda 1962-2061 Bermuda
304 22 304 Greenland 223040 Greenland 1962-2061 Greenland
666 22 666 St.Pierre,Mq 226660 St.Pierre Mq 1962-2061 Saint Pierre and Miquelon
32 33 032 Argentina 330320 Argentina 1962-2061 Argentina
68 33 068 Bolivia 330680 Bolivia 1962-2061 Bolivia
76 33 076 Brazil 330760 Brazil 1962-2061 Brazil
152 33 152 Chile 331520 Chile 1962-2061 Chile
170 33 170 Colombia 331700 Colombia 1962-2061 Colombia
218 33 218 Ecuador 332180 Ecuador 1962-2061 Ecuador
218 33 473 LAIA NES 338960 LAIA NES 1962-2061 LAIA Not Elsewhere Specified
484 33 484 Mexico 334840 Mexico 1962-2061 Mexico
600 33 600 Paraguay 336000 Paraguay 1962-2061 Paraguay
604 33 604 Peru 336040 Peru 1962-2061 Peru
858 33 858 Uruguay 338580 Uruguay 1962-2061 Uruguay
862 33 862 Venezuela 338620 Venezuela 1962-2061 Venezuela
188 34 188 Costa Rica 341880 Costa Rica 1962-2061 Costa Rica
222 34 222 El Salvador 342220 El Salvador 1962-2061 El Salvador
320 34 320 Guatemala 343200 Guatemala 1962-2061 Guatemala
340 34 340 Honduras 343400 Honduras 1962-2061 Honduras
340 34 471 CACM NES 348960 CACM NES 1962-2061 CACM Not Elsewhere Specified
558 34 558 Nicaragua 345580 Nicaragua 1962-2061 Nicaragua
28 35 028 Antigua,Barb 356580 St.Kt-Nev-An 1962-2061 Antigua and Barbuda
44 35 044 Bahamas 350440 Bahamas 1962-2061 Bahamas
52 35 052 Barbados 350520 Barbados 1962-2061 Barbados
92 35 092 Br.Virgin Is 356580 St.Kt-Nev-An 1962-2061 British Virgin Islands
92 35 129 Carib. NES 358960 Carib. NES 1962-2061 Caribbean Not Elsewhere Specified
136 35 136 Cayman Is 353880 Jamaica 1962-2061 Cayman Islands
192 35 192 Cuba 351920 Cuba 1962-2061 Cuba
212 35 212 Dominica 356580 St.Kt-Nev-An 1962-2061 Dominica
214 35 214 Dominican Rp 352140 Dominican Rp 1962-2061 Dominican Republic
308 35 308 Grenada 356580 St.Kt-Nev-An 1962-2061 Grenada
312 35 312 Guadeloupe 353120 Guadeloupe 1962-2061 Guadeloupe
332 35 332 Haiti 353320 Haiti 1962-2061 Haiti
388 35 388 Jamaica 353880 Jamaica 1962-2061 Jamaica
474 35 474 Martinique 353120 Guadeloupe 1962-1995 Martinique
500 35 500 Montserrat 356580 St.Kt-Nev-An 1962-2061 Montserrat
500 35 530 Neth.Antiles 355320 Neth.Ant.Aru 1988-2061 Netherlands Antilles
532 35 532 Neth.Ant.Aru 355320 Neth.Ant.Aru 1962-1987 Frm Neth. Antilles/ Aruba (until 1988)
532 35 533 Aruba 355320 Neth.Ant.Aru 1988-2061 Aruba
658 35 658 St.Kt-Nev-An 356580 St.Kt-Nev-An 1962-1980 Frm St.Kitts/Nev/Anguilla (until 1980)
658 35 659 St.Kitts-Nev 356580 St.Kt-Nev-An 1981-2061 Saint Kitts and Nevis
660 35 660 Anguilla 356580 St.Kt-Nev-An 1981-2061 Anguilla
662 35 662 St.Lucia 356580 St.Kt-Nev-An 1962-2061 Saint Lucia
670 35 670 St.Vincent,G 356580 St.Kt-Nev-An 1962-2061 Saint Vincent and the Grenadines
780 35 780 Trinidad Tbg 357800 Trinidad Tbg 1962-2061 Trinidad and Tobago
796 35 796 Turks,Caicos 353880 Jamaica 1962-2061 Turks and Caicos Islands
850 35 850 US.Virgin Is 218400 USA 1962-1980 Frm US Virgin Islands (until 1980)
80 36 080 Br.Antr.Terr 360800 Br.Antr.Terr 1962-2061 British Antartic Territories
84 36 084 Belize 360840 Belize 1962-2061 Belize
238 36 238 Falkland Is 362380 Falkland Is 1962-2061 Falkland Islands
254 36 254 Fr.Guiana 362540 Fr.Guiana 1962-1995 French Guiana
328 36 328 Guyana 363280 Guyana 1962-2061 Guyana
590 36 590 Fm Pan.Ex-CZ 365900 Panama 1962-1977 Frm Panama ex.Canal Zone (until 1977)
590 36 591 Panama 365900 Panama 1978-2061 Panama
592 36 592 Fm Panama CZ 365900 Panama 1962-1977 Frm Panama Canal Zone (until 1977)
592 36 636 Amer.Rest NS 368960 US NES 1962-2061 Rest of America Not Elsewhere Specified
740 36 740 Suriname 367400 Suriname 1962-2061 Suriname
376 41 376 Israel 413760 Israel 1962-2061 Israel
392 41 392 Japan 413920 Japan 1962-2061 Japan
48 44 048 Bahrain 440480 Bahrain 1962-2061 Bahrain
196 44 196 Cyprus 441960 Cyprus 1962-2061 Cyprus
274 44 275 Occ.Pal.Terr 442750 Occ.Pal.Terr 2000-2061 Occupied Palestinian Territory
364 44 364 Iran 443640 Iran 1962-2061 Iran, Islamic Republic of
368 44 368 Iraq 443680 Iraq 1962-2061 Iraq
400 44 400 Jordan 444000 Jordan 1962-2061 Jordan
414 44 414 Kuwait 444140 Kuwait 1962-2061 Kuwait
422 44 422 Lebanon 444220 Lebanon 1962-2061 Lebanon
512 44 512 Oman 445120 Oman 1962-2061 Oman
536 44 536 Neutral Zone 445360 Neutral Zone 1962-2061 Neutral Zone
634 44 634 Qatar 446340 Qatar 1962-2061 Qatar
682 44 682 Saudi Arabia 446820 Saudi Arabia 1962-2061 Saudi Arabia
720 44 720 Fm Yemen Dm 447200 Fm Yemen Dm 1962-1990 Frm Democratic Yemen (until 1990)
760 44 760 Syria 447600 Syria 1962-2061 Syrian Arab Republic
784 44 784 Untd Arab Em 447840 Untd Arab Em 1962-2061 United Arab Emirates
792 44 792 Turkey 447920 Turkey 1962-2061 Turkey
792 44 879 Asia West NS 448790 Asia West NS 1962-2061 Asia West Not Elsewhere Specified
886 44 886 Fm Yemen AR 448860 Fm Yemen AR 1962-1990 Frm Arab Rep. of Yemen (until 1990)
886 44 887 Yemen 448870 Yemen 1991-2061 Yemen
4 45 004 Afghanistan 450040 Afghanistan 1962-2061 Afghanistan
50 45 050 Bangladesh 450500 Bangladesh 1972-2061 Bangladesh
64 45 064 Bhutan 450000 Asia NES 1962-2061 Bhutan
96 45 096 Brunei Darsm 450000 Asia NES 1962-2061 Brunei Darussalam
104 45 104 Myanmar 451040 Myanmar 1962-2061 Myanmar
116 45 116 Cambodia 451160 Cambodia 1962-2061 Cambodia
144 45 144 Sri Lanka 451440 Sri Lanka 1962-2061 Sri Lanka
156 45 156 China 481560 China 1962-2061 China
900 90 838 Free Zones* 481561 China FTZ 1984-2061 China Foreign Trade Zone
908 90 839 Spec Cats* 481562 China SC 1984-2061 China Special Categories
898 90 899 Areas NES* 481563 China NES 1984-2061 China Areas NES
344 45 344 China HK SAR 453440 China HK SAR 1962-2061 Hong Kong Special Administrative Region
356 45 356 India Ex Sik 453560 India 1962-1974 Frm India excl. Sikkim (until 1974)
360 45 360 Indonesia 453600 Indonesia 1962-2061 Indonesia
408 45 408 Korea D P Rp 484080 Korea D P Rp 1962-2061 Korea, Democratic People's Republic of
410 45 410 Korea Rep. 454100 Korea Rep. 1962-2061 Korea, Republic of
418 45 418 Lao P.Dem.R 454180 Lao P.Dem.R 1962-2061 Lao People's Democratic Republic
446 45 446 China MC SAR 454460 China MC SAR 1962-2061 Macau Special Administrative Region
446 45 457 Sarawak 454580 Malaysia 1962-1963 Frm Sarawak (until 1963)
458 45 458 Malaysia 454580 Malaysia 1964-2061 Malaysia
458 45 459 Pen Malaysia 454580 Malaysia 1962-1963 Frm Peninsula Malaysia (until 63)
458 45 461 Sabah 454580 Malaysia 1962-1963 Frm Sabah (until 1963)
462 45 462 Maldives 453600 Indonesia 1962-2061 Maldives
896 45 490 Asia Othr.NS 458960 Taiwan 1962-2061 Asia Other Not Elsewhere Specified
496 45 496 Mongolia 484960 Mongolia 1962-2061 Mongolia
524 45 524 Nepal 455240 Nepal 1962-2061 Nepal
586 45 586 Pakistan 455860 Pakistan 1972-2061 Pakistan
586 45 588 Pakistan E/W 455860 Pakistan 1962-1971 Frm Pakistan East/West (until 71)
608 45 608 Philippines 456080 Philippines 1962-2061 Philippines
626 45 626 East Timor 453600 Indonesia 1962-2061 East Timor
650 45 647 Ryukyu Is 413920 Japan 1962-1972 Frm Ryukyu Island (until 1972)
698 45 698 Sikkim 453560 India 1962-1974 Frm Sikkim (until 1974)
698 45 699 India 453560 India 1975-2061 India
702 45 702 Singapore 457020 Singapore 1962-2061 Singapore
704 45 704 Viet Nam 487040 Viet Nam 1975-2061 Viet Nam
764 45 764 Thailand 457640 Thailand 1962-2061 Thailand
866 45 866 Fm Vietnm DR 487040 Viet Nam 1962-1974 Frm Viet Nam Democratic (until 1974)
868 45 868 Fm Vietnm Rp 487040 Viet Nam 1962-1974 Frm Viet Nam Republic (until 1974)
868 46 031 Azerbaijan 460310 Azerbaijan 1992-2061 Azerbaijan
868 46 051 Armenia 460510 Armenia 1992-2061 Armenia
868 46 268 Georgia 462680 Georgia 1992-2061 Georgia
868 46 398 Kazakhstan 463980 Kazakhstan 1992-2061 Kazakhstan
868 46 417 Kyrgyzstan 464170 Kyrgyzstan 1992-2061 Kyrgyzstan
868 46 762 Tajikistan 467620 Tajikistan 1992-2061 Tajikistan
868 46 795 Turkmenistan 467950 Turkmenistan 1992-2061 Turkmenistan
868 46 860 Uzbekistan 468600 Uzbekistan 1992-2061 Uzbekistan
40 53 040 Austria 550400 Austria 1962-2061 Austria
40 53 056 Belgium 530560 Belgium-Lux 1999-2061 Belgium
56 53 058 Belgium-Lux 530560 Belgium-Lux 1962-2061 Belgium, Luxembourg
208 53 208 Denmark 532080 Denmark 1973-2061 Denmark
208 53 246 Finland 552460 Finland 1995-2061 Finland
250 53 251 France,Monac 532500 France,Monac 1962-2061 France, Monaco and Overseas territories
250 53 276 Germany 532760 Germany 1991-2061 Germany
280 53 280 Fm German FR 532800 Fm German FR 1962-1990 Frm Germany, Federal (until 1990)
300 53 300 Greece 533000 Greece 1981-2061 Greece
372 53 372 Ireland 533720 Ireland 1973-2061 Ireland
674 53 381 Italy 533800 Italy 1962-2061 Italy, incl. San Marino and the Holy See
380 53 381 Italy 533800 Italy 1962-2061 Italy, incl. San Marino and the Holy See
442 53 442 Luxembourg 530560 Belgium-Lux 1999-2061 Luxembourg
442 53 492 Europe EU NS 538960 EEC NES 1962-2061 European Union Not Elsewhere Specified
528 53 528 Netherlands 535280 Netherlands 1962-2061 Netherlands
620 53 620 Portugal 536200 Portugal 1986-2061 Portugal
724 53 724 Spain 537240 Spain 1986-2061 Spain
752 53 752 Sweden 557520 Sweden 1962-2061 Sweden
826 53 826 UK 538260 UK 1973-2061 United Kingdom
246 55 246 Finland 552460 Finland 1962-1994 Finland
352 55 352 Iceland 553520 Iceland 1971-2061 Iceland
578 55 579 Norway,Sb,JM 555780 Norway 1962-2061 Norway
578 55 697 Eur. EFTA NS 558960 Eur. EFTA NS 1962-2061 Europe EFTA Not Elsewhere Specified
438 55 757 Switz.Liecht 557560 Switz.Liecht 1962-2061 Switzerland, Liechtenstein
756 55 757 Switz.Liecht 557560 Switz.Liecht 1962-2061 Switzerland, Liechtenstein
8 56 008 Albania 580080 Albania 1962-2061 Albania
100 56 100 Bulgaria 581000 Bulgaria 1962-2061 Bulgaria
200 56 200 Czechoslovak 582000 Czechoslovak 1962-1992 Frm Czechoslovakia (until 1992)
200 56 203 Czech Rep 582030 Czech Rep 1993-2061 Czech Republic
200 56 221 Eur. East NS 582210 E Europe NES 1962-2061 Europe East Not Elsewhere Specified
278 56 278 Fm German DR 582780 Fm German DR 1962-1990 Frm Germany, Democratic (until 90)
348 56 348 Hungary 583480 Hungary 1962-2061 Hungary
616 56 616 Poland 586160 Poland 1962-2061 Poland
642 56 642 Romania 586420 Romania 1962-2061 Romania
642 56 703 Slovakia 587030 Slovakia 1993-2061 Slovakia
20 57 020 Andorra 532500 France,Monac 1962-2061 Andorra
234 57 234 Faeroe Is 532080 Denmark 1962-2061 Faeroe Islands
292 57 292 Gibraltar 572920 Gibraltar 1962-2061 Gibraltar
470 57 470 Malta 574700 Malta 1962-2061 Malta
470 57 568 Eur. Othr.NS 578960 Eur. Other NES 1962-2061 Europe Other Not Elsewhere Specified
890 57 890 Fm Yugoslav 598900 Fm Yugoslav 1962-1991 Frm SFR Yugoslavia (until 1991)
890 58 112 Belarus 581120 Belarus 1992-2061 Belarus
890 58 233 Estonia 582330 Estonia 1992-2061 Estonia
890 58 428 Latvia 584280 Latvia 1992-2061 Latvia
890 58 440 Lithuania 584400 Lithuania 1992-2061 Lithuania
890 58 498 Rep Moldova 584980 Rep Moldova 1992-2061 Republic of Moldova
890 58 643 Russian Fed 586430 Russian Fed 1992-2061 Russian Federation
890 58 804 Ukraine 588040 Ukraine 1992-2061 Ukraine
890 59 070 Bosnia Herzg 590700 Bosnia Herzg 1992-2061 Bosnia and Herzegovina
890 59 191 Croatia 591910 Croatia 1992-2061 Croatia
890 59 705 Slovenia 597050 Slovenia 1992-2061 Slovenia
890 59 807 TFYR Macedna 598070 TFYR Macedna 1993-2061 TFYR Macedonia
890 59 891 Yugoslavia 598910 Yugoslavia 1992-2061 Yugoslavia
810 68 810 Fm USSR 688100 Fm USSR 1962-1991 Frm USSR (until 1991)
36 71 036 Australia 710360 Australia 1962-2061 Australia
554 71 554 New Zealand 715540 New Zealand 1962-2061 New Zealand
90 72 090 Solomon Is 722960 Kiribati 1962-2061 Solomon Islands
162 72 162 Christmas Is 710360 Australia 1962-2061 Christmas Islands
166 72 166 Cocos Is 710360 Australia 1962-2061 Cocos (Keeling) Islands
184 72 184 Cook Is 715540 New Zealand 1962-2061 Cook Islands
242 72 242 Fiji 722420 Fiji 1962-2061 Fiji
258 72 258 Fr.Polynesia 725400 New Calednia 1962-2061 French Polynesia
296 72 296 Kiribati 722960 Kiribati 1962-2061 Kiribati
520 72 520 Nauru 722420 Fiji 1962-2061 Nauru
520 72 527 Oceania NES 728960 Oth.Oceania NES 1962-2061 Oceania Not Elsewhere Specified
540 72 540 New Calednia 725400 New Calednia 1962-2061 New Caledonia
548 72 548 Vanuatu 722960 Kiribati 1962-2061 Vanuatu
570 72 570 Niue 715540 New Zealand 1962-2061 Niue
574 72 574 Norfolk Is 710360 Australia 1962-2061 Norfolk Islands
574 72 580 N.Mariana Is 728960 Oth.Oceania NES 1992-2061 Northern Mariana Islands
582 72 582 Fm Pacific I 368960 US NES 1962-1991 Frm Pacific Islands (until 1991)
16 72 583 Micronesia 368960 US NES 1992-2061 Micronesia, Federated States of
16 72 584 Marshall Is 368960 US NES 1992-2061 Marshall Islands
16 72 585 Palau 368960 US NES 1992-2061 Palau
598 72 598 Papua N.Guin 725980 Papua N.Guin 1962-2061 Papua New Guinea
612 72 612 Pitcairn 722960 Kiribati 1962-2061 Pitcairn
772 72 772 Tokelau 715540 New Zealand 1962-2061 Tokelau
776 72 776 Tonga 722420 Fiji 1962-2061 Tonga
798 72 798 Tuvalu 722960 Kiribati 1962-2061 Tuvalu
316 72 849 US Msc.Pac.I 368960 US NES 1962-2061 US Miscellaneous Pacific Islands
876 72 876 Wallis Fut.I 725400 New Calednia 1962-2061 Wallis and Futuna Islands
882 72 882 Samoa 728882 Samoa 1962-2061 Samoa
904 90 837 Bunkers 908960 Areas NES 1962-2061 Bunkers
900 90 838 Free Zones 908960 Areas NES 1962-2061 Free Zones
908 90 839 Spec Cats 908960 Areas NES 1962-2061 Special Categories
898 90 899 Areas NES 908960 Areas NES 1962-2061 Not Specified
