function [IE,A,A0D,years,names] = extractDataForCountries( data, cn, desiredCountries )

% Returns
% IE: import/export for countries (may not be symmetric)
% A: symmetrized (IE+IE') 
% A0D: Just like A, but with 0 on the diagonal
% years: years

ind = getCountryCodeIndexFromList( cn, desiredCountries );
names = cn(ind);

nc = length( desiredCountries );
ny = length( data );

IE = zeros( nc, nc, ny );
A = zeros( nc, nc, ny );
A0D = zeros( nc, nc, ny );
years = zeros( ny, 1 );

for i=1:ny
  IE(:,:,i) = data(i).IE( ind, ind );
  cA = data(i).A( ind, ind );
  A(:,:,i) = cA;
  cA0D = cA;
  cA0D( logical( eye( nc ) ) ) = zeros( nc, 1 );
  A0D(:,:,i) = cA0D;
  years(i) = data(i).year;
end