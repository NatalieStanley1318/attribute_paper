function ind = getCountryCodeIndexFromList( cn, cd )

[~,ind] = intersect( cn, cd );
