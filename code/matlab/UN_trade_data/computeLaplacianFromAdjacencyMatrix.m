function L = computeLaplacianFromAdjacencyMatrix( A )

% zero out the diagonal just to be sure
n = length(A);
A( logical( eye( n ) ) ) = zeros( n, 1 );


% degree matrix
D = zeros( n );
for i=1:n
    D(i,i) = sum( A(i,:) );
end

% now compute the Laplace matrix
L = D-A;