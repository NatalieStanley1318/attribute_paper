function [cc,cn] = parseCountryCodes( fName )

cc = {};
cn = {};

fid = fopen( fName, 'rt' );
if ( fid<0 )
  error( 'Could not open file\n' );
end

C = textscan( fid, '%d %d %d %[^0123456789] %d %[^0123456789] %d-%d %[^\n]' );

cc_all = C{5};
cn_all = C{6};

[cc,ind] = unique( cc_all );
cn = cn_all(ind);
for i=1:length( cn )
  cn{i} = strtrim( cn{i} );
end

fclose( fid );

