function [A,B,P_intercept,P_slope] = computeLinearMixedEffectsModel( d )
  
sz = size( d(1).data(1).M_ratio_to_region_fibers );

P_slope = NaN*ones( sz );
P_intercept = NaN*ones( sz );

A = zeros( sz );
B = zeros( sz );

for s1=1:sz(1)
  for s2=1:min( s1-1, sz(2) )
   
    con = [];
    ts = [];
    sid = [];
    
    for i=1:length( d )
      for j=1:length( d(i).data )
        
        con = [con; d(i).data(j).M_ratio_to_region_fibers(s1,s2)];
        ts = [ts; d(i).data(j).tp];
        sid = [sid; d(i).subjectId];
        
      end
    end

    tbl = table( con, ts, sid, 'VariableNames', {'con', 't', 'sid'} );
    lme = fitlme( tbl, 'con~t + (t|sid)' );
     
    ps = lme.Coefficients.pValue;
    P_slope(s1,s2) = ps(2);
    P_intercept(s1,s2) = ps(1);
    fe = lme.fixedEffects;
    A(s1,s2) = fe(2);
    B(s1,s2) = fe(1);
        
  end
end
