function [] = visualizeConnectivity( ds, diffMode, maxPerRow )

if ( nargin<2 )
    diffMode = true;
end

if ( nargin<3 )
    maxPerRow = 5;
end

nrOfTimePoints = length( ds.data );

nrOfCols = min( maxPerRow, nrOfTimePoints );
nrOfRows = ceil( nrOfTimePoints/maxPerRow );

set( gcf, 'name', sprintf( 'Subject id = %d', ds.subjectId ) );

for i=1:nrOfTimePoints
    subplot( nrOfRows, nrOfCols, i );
    if ( ( i==1 ) | ~diffMode )
       imagesc( ds.data(i).M );
    else
       imagesc( ds.data(i).M - ds.data(1).M );
    end
    axis square
    colorbar
    title( sprintf( 't = %d', ds.data(i).tp ) );
end
    


