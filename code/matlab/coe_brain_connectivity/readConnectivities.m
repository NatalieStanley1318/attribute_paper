function retF = readConnectivities( nrOfMinTimePoints )

if ( nargin<1 )
    nrOfMinTimePoints = 0;
end

d = dir( 'data/*final.csv' );

ret = [];

for i=1:length( d )
    fprintf('Reading %s ... \n', d(i).name );
    %CC = csvread( d(i).name, 2, 0 ); % cannot use due to data format
    %issues
    C = mycsvread( ['data/' d(i).name], 3 );
    if ( isempty( C ) )
      fprintf('ERROR reading %s. Skipping\n', d(i).name );
    else
      CM = convertToConnectivityMatrix( C );
      [subjectID,timePoint] = getSubjectIDAndTimepointFromFilename( d(i).name );    
      ret = addSubjectInformationToDataStructure( ret, CM, subjectID, timePoint );
    end
end

% compute number of available timepoints per subject
nt = computeNumberOfAvailableTimePointsPerSubject( ret );

% filter based on number of timepoints
retF = ret( nt>=nrOfMinTimePoints );