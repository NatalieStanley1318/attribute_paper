function MN = normalizeC_ratioToRegionFibers( C )

MN = C;
sz = size( MN );
for i=1:sz(1)
    for j=1:sz(2)
        MN(i,j) = MN(i,j)/( sum( C(i,:) ) + sum( C(:,j) ) );
    end
end