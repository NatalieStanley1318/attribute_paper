clear all
close all

addpath ../export_fig

labels = {'R Occipital',...
'R Temporal Auditory',...
'R Subcortical',...
'R Frontal',...
'R Cerebellum',...
'R Insula',...
'R Cingulate',...
'R Parietal',...
'R Prefrontal',...
'R Corpus Callosum',...
'R Temporal Visual',...
'R Temporal Limbic',...
'R Pons and Medulla',...
'L Occipital',...
'L Temporal Auditory',...
'L Subcortical',...
'L Frontal',...
'L Cerebellum',...
'L Insula',...
'L Cingulate',...
'L Parietal',...
'L Prefrontal',...
'L Corpus Callosum',...
'L Temporal Visual',...
'L Temporal Limbic',...
'L Pons and Medulla',...
'R Lat Ventricle',...
'L Lat Ventricle'};

d = readConnectivities( 0 );

[A_cf,B_cf,P_intercept_cf,P_slope_cf] = computeTimeAverage( d );
[A_lme,B_lme,P_intercept_lme,P_slope_lme] = computeLinearMixedEffectsModel( d );
%load lmeRes  % pre-computed, so we can quickly play with it

fdrAlpha = 0.05;

pFDRThresh_P_int_cf = FDR( P_intercept_cf, fdrAlpha );
pFDRThresh_P_slope_cf = FDR( P_slope_cf, fdrAlpha );
pFDRThresh_P_int_lme = FDR( P_intercept_lme, fdrAlpha );
pFDRThresh_P_slope_lme = FDR( P_slope_lme, fdrAlpha );

doPrint = true;

figure
imagesc( A_cf ), axis square, hc = colorbar;
ht = title('cross-sectional model, slope A');
adjustPlotAppearance( labels, ht, hc );
if ( doPrint )
    export_fig  -m2 cross_sectional_linear_model_slope_A.png
end

figure
imagesc( A_lme ), axis square, hc = colorbar;
ht = title('mixed effects model, slope A');
adjustPlotAppearance( labels, ht, hc );
if ( doPrint )
    export_fig -m2 linear_mixed_effects_model_slope_A.png
end

figure
imagesc( B_cf ), axis square, hc = colorbar;
ht = title('cross-sectional linear model, intercept B');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 cross_sectional_linear_model_intercept_B.png
end

figure
imagesc( B_lme ), axis square, hc = colorbar;
ht = title('mixed effects model, intercept B');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 linear_mixed_effects_model_intercept_B.png
end

figure
imagesc( -log10( P_intercept_cf ) ), axis square, hc = colorbar;
ht = title('-log10( P cross-sectional intercept B)');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 cross_sectional_linear_model_intercept_B_log10P.png
end

figure
imagesc( -log10( P_intercept_lme) ), axis square, hc = colorbar;
ht = title('-log10( P mixed-effect intercept B )');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 linear_mixed_effects_model_intercept_B_log10P.png
end

figure
imagesc( P_intercept_cf <= pFDRThresh_P_int_cf ), axis square, hc = colorbar;
ht = title('FDR significant( P cross-sectional intercept B )');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 cross_sectional_linear_model_FDR_significant_intercept_B.png
end

figure
imagesc( P_intercept_lme <= pFDRThresh_P_int_lme ), axis square, hc = colorbar;
ht = title('FDR significant( P mixed-effect intercept B )');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 linear_mixed_effects_model_FDR_significant_intercept_B.png
end

figure
imagesc( -log10( P_slope_cf ) ), axis square, hc = colorbar;
ht = title('-log10( P cross-sectional slope A)');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 cross_sectional_linear_model_slope_A_log10P.png
end

figure
imagesc( -log10( P_slope_lme) ), axis square, hc = colorbar;
ht = title('-log10( P mixed-effect slope A )');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 linear_mixed_effects_model_slope_A_log10P.png
end

figure
imagesc( P_slope_cf <= pFDRThresh_P_int_cf ), axis square, hc = colorbar;
ht = title('FDR significant( P cross-sectional slope A )');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 cross_sectional_linear_model_FDR_significant_slope_A.png
end

figure
imagesc( P_slope_lme <= pFDRThresh_P_int_lme ), axis square, hc = colorbar;
ht = title('FDR significant( P mixed-effect slope A )');
adjustPlotAppearance( labels, ht, hc );

if ( doPrint )
    export_fig -m2 linear_mixed_effects_model_FDR_significant_slope_A.png
end
