function CM = convertToConnectivityMatrix( C )

maxRow = max( C(:,1) );
maxCol = max( C(:,2) );

overallMax = max( maxRow, maxCol );

CM = zeros( overallMax );

for i=1:size( C, 1 )
    CM( C(i,1), C(i,2) ) = C( i, 3 );
end

fprintf( '  --> Created connectivity matrix of size (%d,%d)\n', size( CM, 1 ), size( CM, 2 ) );
