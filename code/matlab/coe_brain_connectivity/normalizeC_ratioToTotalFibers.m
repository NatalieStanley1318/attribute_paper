function MN = normalizeC_ratioToTotalFibers( C )

MN = C/sum(C(:));

