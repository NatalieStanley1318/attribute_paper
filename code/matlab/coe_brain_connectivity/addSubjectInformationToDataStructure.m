function retOut = addSubjectInformationToDataStructure( ret, C, id, tp )

if ( isempty( ret ) )
    retOut.subjectId = id;
    retOut.data.tp = tp;
    retOut.data.M = C;
    retOut.data.M_ratio_to_total_fibers = normalizeC_ratioToTotalFibers( C );
    retOut.data.M_ratio_to_region_fibers = normalizeC_ratioToRegionFibers( C );
    return;
end

retOut = ret;

if ( ~isempty( retOut ) )
    [idExists,indx] = checkIfSubjectIDExists( retOut, id );
    
    if ( ~idExists )
       retOut(end+1).subjectId = id;
       retOut(end).data.tp = tp;
       retOut(end).data.M = C;
       retOut(end).data.M_ratio_to_total_fibers = normalizeC_ratioToTotalFibers( C );
       retOut(end).data.M_ratio_to_region_fibers = normalizeC_ratioToRegionFibers( C );
    else % idExists == true
       retOut(indx).data(end+1).tp = tp;
       retOut(indx).data(end).M = C;
       retOut(indx).data(end).M_ratio_to_total_fibers = normalizeC_ratioToTotalFibers( C );
       retOut(indx).data(end).M_ratio_to_region_fibers = normalizeC_ratioToRegionFibers( C );
       retOut(indx).data = sortTimes( retOut(indx).data );
    end
end

function dR = sortTimes( d )

dR = d;
tps = [d.tp];
[vs,indx] = sort( tps );
for i=1:length( tps )
    dR(i).tp = d( indx(i) ).tp;
    dR(i).M = d( indx(i) ).M;
    dR(i).M_ratio_to_total_fibers = d( indx(i) ).M_ratio_to_total_fibers;
    dR(i).M_ratio_to_region_fibers = d( indx(i) ).M_ratio_to_region_fibers;
end

function [idExists,indx] = checkIfSubjectIDExists( ret, id )

idExists = false;
indx = -1;

for i=1:length( ret )
    if ( ret(i).subjectId == id )
        indx = i;
        idExists = true;
        break;
    end
end