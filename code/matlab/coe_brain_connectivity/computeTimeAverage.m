function [A,B,P_intercept,P_slope, Tstat_intercept, Tstat_slope] = computeTimeAverage( d )
  
% computes a least square fit over all matrices
% Model is M(t) = At + B, all matrix valued
  
% first create matrix with timepoints
  
T = [];
Y = [];

sz = size( d(1).data(1).M_ratio_to_region_fibers );
n = 0;

for i=1:length( d )
  for j=1:length( d(i).data )
    T = [T; d(i).data(j).tp 1];
    Y = [Y; ( d(i).data(j).M_ratio_to_region_fibers(:) )'];
    n = n + 1;
  end
end

% now solve it

len = size(Y,2);
AB = T\Y;
    
A = AB(1,:);
B = AB(2,:);

A = reshape( A, sz );
B = reshape( B, sz );

[avgM,avgT,sumT_sqr] = computeAverage( d );

% now compute squared residuals with respect to the fit model
M_sR_fit = zeros( sz );
for i=1:length( d )
  for j=1:length( d(i).data )
    M_sR_fit = M_sR_fit + ( d(i).data(j).M_ratio_to_region_fibers - (A*d(i).data(j).tp + B ) ).^2;
  end
end

% and squared residuals with respect to the average time
tp_sR = 0;
for i=1:length( d )
  for j=1:length( d(i).data )
      tp_sR = tp_sR + ( d(i).data(j).tp-avgT ).^2;
  end
end

% compute th variance of the intercept estimat
varB = (M_sR_fit/(n-2))/tp_sR/n*sumT_sqr;
% use it to standardize th coefficients to get the t-values
Tstat_intercept = B./sqrt( varB );
% now compute the p-value for the intercept
P_intercept = 2*tcdf( abs( Tstat_intercept ), n-2, 'upper' );

% compute the variance of the slope estimate
varA = (M_sR_fit/(n-2))/tp_sR;
% use it to standardize the coefficients to get t-values
Tstat_slope = A./sqrt( varA );

% now compute the p-value for the tstatistics
P_slope = 2*tcdf( abs( Tstat_slope ), n-2, 'upper' );

% force the upper triangular parts to NaN
for s1=1:sz(1)
    for s2=s1:sz(2)
        P_slope( s1, s2 ) = NaN;
        P_intercept( s1, s2 ) = NaN;
        A( s1, s2 ) = NaN;
        B( s1, s2 ) = NaN;
    end
end
    
    