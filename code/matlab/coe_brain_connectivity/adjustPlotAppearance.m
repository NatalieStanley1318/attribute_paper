function [] = adjustPlotAppearance( labels, ht, hc )

set( ht, 'fontsize', 20 );
set( hc, 'fontsize', 20 );
set( gca, 'xtick', 1:28 );
set( gca, 'ytick', 1:28 );
set( gca, 'xticklabel', labels );
set( gca, 'xticklabelrotation', -40 );
set( gca, 'yticklabel', labels );
set( gca, 'yticklabelrotation', 40 );
set( gcf, 'color', [1 1 1] );