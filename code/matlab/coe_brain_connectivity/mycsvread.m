function R = mycsvread( fn, skiprow )

R = [];

fid = fopen( fn, 'rt' );

for i=1:skiprow
    fgetl( fid );
end

while ~feof( fid )
    l = fgetl( fid );
    % first remove all the quotes (due to mistake in data generation)
    l = l( l~='"' );
    commas = strfind(l,',' );
    cv = [];
    if ( ~isempty( commas ) )
        cv = str2num( l(1:commas(1)-1) );
        for i=2:length(commas)
            cv = [cv, str2num( l(commas(i-1)+1:commas(i)-1 ) )];
        end
        cv = [cv, str2num( l(commas(end)+1:end) )];
    else
        cv = [cv, str2num( l )];
    end
    R = [R;cv];
end

fclose( fid );