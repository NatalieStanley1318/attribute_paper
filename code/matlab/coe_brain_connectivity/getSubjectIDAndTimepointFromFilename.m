function [id,tp] = getSubjectIDAndTImepointFromFilename( fn )

id = -1; 
tp = -1;

indx = strfind( fn, '_' );
if ( isempty( indx ) )
    error( 'Malformed filename string' );
end

id = str2num( fn( 1:indx(1)-1 ) );

indxM = strfind( fn, 'months' );
indxW = strfind( fn, 'weeks' );
if ( isempty( indxM ) & isempty( indxW ) )
    error( 'Malformed filename string' );
end

if ( ~isempty( indxM ) )
    tp = str2num( fn( indx(1)+1:indxM(1)-1 ) );
else
    tp = str2num( fn( indx(1)+1:indxW(1)-1 ) );
    tp = tp/4; % assuming 4 weeks per month
end


fprintf('  --> Found subject id = %d and timepoint %d\n', id, tp );