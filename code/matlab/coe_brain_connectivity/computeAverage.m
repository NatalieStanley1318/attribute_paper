function [A,t,sum_t_sqr] = computeAverage( d )
  
% computes the average of all matrices in d
% first create matrix with timepoints

sz = size( d(1).data(1).M );
A = zeros( sz );
t = 0;
sum_t_sqr = 0;
n = 0;

for i=1:length( d )
  for j=1:length( d(i).data )
      A = A + d(i).data(j).M;
      t = t + d(i).data(j).tp;
      sum_t_sqr = sum_t_sqr + d(i).data(j).tp^2;
      n = n + 1;
  end
end

A = A/n;
t = t/n;