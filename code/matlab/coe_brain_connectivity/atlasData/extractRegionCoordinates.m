clear all
close all

% data is isotropic 0.65mm^3
im = nrrdLoad( 'SEG_T1_12months_Atlas_ANTS_PARC_Prop2COEAtlasSpace.nrrd');

labelIDs = unique( im(:) );
labelIDs = labelIDs( labelIDs~=0 );

sz = size( im );

idsAndCoors = [];

for i=1:length( labelIDs )
  
  currentLabelID = labelIDs(i);
  fprintf( 'Processing label ID=%d\n', i );
  indx= find( im==currentLabelID );
  [x,y,z] = ind2sub( sz, indx );
  
  currentCentroid = [mean(x), mean(y), mean(z)]*0.65;
  idsAndCoors = [idsAndCoors; i, currentCentroid];
  
end

figure
plot3( idsAndCoors(:,2), idsAndCoors(:,3), idsAndCoors(:,4), 'r*' );
axis equal

csvwrite( 'centroidsForIDs.txt', idsAndCoors );

