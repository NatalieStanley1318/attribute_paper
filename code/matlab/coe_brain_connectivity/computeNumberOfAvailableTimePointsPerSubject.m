function nt = computeNumberOfAvailableTimePointsPerSubject( ret )

nt = [];

for i=1:length( ret )
    nt = [nt; length( ret(i).data )];
end