function [idFrom,idTo,weights,timesteps] = readMITData( fileName )

fid = fopen( fileName, 'rt' );

if ( fid<0 )
    error( 'Could not open file\n' );
end

% skip first line
fgetl( fid );

chunkSize = 100000;

idFrom = zeros( chunkSize, 1 );
idTo = zeros( chunkSize, 1 );
weights = zeros( chunkSize, 1 );
timesteps = zeros( chunkSize, 1 );

iI = 0;

while ~feof( fid ) 
   iI = iI+1;
   cL = fgetl( fid );
   vals = sscanf( cL, '%f' );
   
   if ( iI>length( idFrom ) )
       fprintf('added chunk to length = %d\n', length( idFrom ) );
       idFrom = [idFrom; zeros( chunkSize, 1 ) ];
       idTo = [idTo; zeros( chunkSize, 1 ) ];
       weights = [weights; zeros( chunkSize, 1 ) ];
       timesteps = [timesteps; zeros( chunkSize, 1 ) ];
   end
   
   idFrom( iI ) = vals(1);
   idTo( iI ) = vals(2);
   weights( iI ) = vals(3);
   timesteps( iI ) = vals(4);
end

idFrom = idFrom( 1:iI );
idTo = idTo( 1:iI );
weights = weights( 1:iI );
timesteps = timesteps( 1:iI );

fclose( fid );