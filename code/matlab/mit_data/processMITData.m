[idFrom,idTo,weights,timesteps] = readMITData( 'out.mit' );

u_idFrom = unique( idFrom );
u_idTo = unique( idTo );
u_timesteps = unique( timesteps );

minId = min( [u_idFrom; u_idTo] );
maxId = max( [u_idFrom; u_idTo] );

numberOfAdjacencyMatrices = length( u_timesteps );

if ( minId~=1 )
    error( 'Assuming that minid == 1\n' );
end

dimensionOfAdjacencyMatrices = maxId; % assuming minId == 1

A = zeros( dimensionOfAdjacencyMatrices, dimensionOfAdjacencyMatrices, numberOfAdjacencyMatrices );

for i=1:numberOfAdjacencyMatrices
    
    indx = find( timesteps == u_timesteps( i ) );
    
    c_idFrom = idFrom( indx );
    c_idTo = idTo ( indx );
    c_weights = weights( indx );
    
    A(:,:,i) = buildAdjacencyMatrix( c_idFrom, c_idTo, c_weights, dimensionOfAdjacencyMatrices );
    
end

% average over 1000 each

A_avg = zeros( dimensionOfAdjacencyMatrices, dimensionOfAdjacencyMatrices, length( 1:1000:numberOfAdjacencyMatrices ) );
i_avg = 0;

for i=1:1000:1:1000:numberOfAdjacencyMatrices
    i_avg = i_avg + 1;
    range = i:min( i+(1000-1), numberOfAdjacencyMatrices );
    A_avg(:,:,i_avg) = sum( A(:,:,range), 3 )/length( range );
end