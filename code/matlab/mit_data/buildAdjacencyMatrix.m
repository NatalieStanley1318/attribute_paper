function A = buildAdjacencyMatrix( idFrom, idTo, weights, dim )

A = zeros( dim );

for i=1:length( idFrom )
    A( idFrom(i), idTo(i) ) = A( idFrom(i), idTo(i) ) + weights( i );
    A( idTo(i), idFrom(i) ) = A( idTo(i), idFrom(i) ) + weights( i );
end