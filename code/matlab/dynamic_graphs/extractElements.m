function [SE,YE,HE,ZE] = extractElements( debugOutput )

Y0 = debugOutput(1).Y;

nt = length( debugOutput );

n = size( Y0, 1 );
p = size( Y0, 2 );

SE = zeros( n, n, nt );
YE = zeros( n, p, nt );
HE = zeros( p, p, nt );
ZE = zeros( n, p, nt );

for iI=1:nt
  
  SE(:,:,iI) = ( debugOutput( iI ).Y )*( debugOutput( iI ).Y )';
  YE(:,:,iI) = debugOutput( iI ).Y;
  HE(:,:,iI) = debugOutput( iI ).H;
  ZE(:,:,iI) = debugOutput( iI ).Z;
  
end