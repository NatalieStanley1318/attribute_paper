function x = getStateVectorFromStateMatricesFixedY0( H, Z )

x = [extractUpperTriangularPartAsVector( H ); Z(:)];

function x = extractUpperTriangularPartAsVector( H )

x= H( triu( ones( size( H ) ) )==1 );