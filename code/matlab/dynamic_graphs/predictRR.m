function prediction = predictRR( b, A, m )
% predictRR Sliding-window based prediction from ridge-regression model.
%
% Author(s): Roland Kwitt, Marc Niethammer
% Date: April, 2015

    T = size( A, 2 ); % time steps
    
    cnt=2; 
    prediction = b( 1 ); % offset
    for t=T-m+1:T % last m time points (not used for training)
        prediction = prediction+b(cnt)*A(:,t);
        cnt = cnt+1;
    end
end