function [xc,debugOutput] = computeDynamicGraphEvolution( xc, nt, sz )

debugOutput = [];
h = 1/nt;

YD = []; HD = []; ZD = [];

if ( nargout>1 ) 
  [YD,HD,ZD] = getStateMatricesFromStateVector( xc, sz );
  debugOutput(1).Y = YD;
  debugOutput(1).H = HD;
  debugOutput(1).Z = ZD;
end

for iI=1:nt
  xc = internal_rhs_rk4( xc, h, sz );
  
  if ( nargout>1 )
    [YD,HD,ZD] = getStateMatricesFromStateVector( xc, sz );
    debugOutput(end+1).Y = YD;
    debugOutput(end).H = HD;
    debugOutput(end).Z = ZD;
  end
  
end

end

function ret = internal_rhs_rk4( x, h, sz )
% Generic rk4 integrator

ret = [];

k1 = rhs_fixed_rank_geodesic( x, sz )*h;
k2 = rhs_fixed_rank_geodesic( x + 1/2*k1, sz )*h;
k3 = rhs_fixed_rank_geodesic( x + 1/2*k2, sz )*h;
k4 = rhs_fixed_rank_geodesic( x +  k3, sz )*h;

ret = x + 1/6*k1 + 1/3*k2 + 1/3*k3 + 1/6*k4;

end

function ret = rhs_fixed_rank_geodesic( x, sz )

Y = [];
H = [];
Z = [];

[Y,H,Z] = getStateMatricesFromStateVector( x, sz );

YTY_inv = inv( Y'*Y );

YP = Y*H + Z;
HP = -2*H*H + 0.5*YTY_inv*(Z'*Z) + 0.5*(Z'*Z)*YTY_inv;
ZP = -3*Z*H - Y*YTY_inv*(Z'*Z);

ret = getStateVectorFromStateMatrices( YP, HP, ZP );

end