clear all; close all

setenv('ADIMAT_HOME', '/afs/cs.unc.edu/home/mn/private/research/programming/matlab/adimat/adimat' )
run('/afs/cs.unc.edu/home/mn/private/research/programming/matlab/adimat/adimat/adimat-0.6.0-4971-GNU_Linux-x86_64/ADiMat_startup.m');

params.n = 5;
params.p = params.n-1;
params.sigma = 0.001;
params.nt = 50;
params.optimizeOverY0 = false;

p = params.p;
n = params.n;

% state vector is composed of Y, upper triangluar part of H (as H is symmetric) and Z
% Y: nxp, H: pxp, Z: nxp

% create a reasonable Laplacian matrix to start and to finish
%[S0,Y0] = createRandomLaplaceMatrix( params.n );
%[SF,YF] = createRandomLaplaceMatrix( params.n );

Y0 = 0.05*randn( n, p );
YF = 0.05*randn( n, p );
S0 = Y0*Y0';
SF = YF*YF';

keyboard

[Y0,H0,Z0] = computeInitialization( Y0, YF );

params.desired_S_TF = SF; % desired Laplace matrix
params.Y0 = Y0;

if ( params.optimizeOverY0 )
  x0 = getStateVectorFromStateMatrices( Y0, H0, Z0 );
else
  x0 = getStateVectorFromStateMatricesFixedY0( H0, Z0 );
end

optOpts = loadOptimizerOptions();

% If code has changed and we need to update the interpolation functions
opts.optsAdimat = admOptions( 'functionResults', {[0]}, 'i', 1 ); %, 'o', 1 ); % one independent and one dependent variable 
opts.params = params; 

%x = fminunc( @(x)dynamicGraphEvolution_adimat_optDriver( x, opts ), x0, optOpts.fminunc );
if ( params.optimizeOverY0 )
  error('Not implemented yet');
else
  beq = zeros( p*p, 1 );
  Aeq = createAeqMatrix( params.Y0 );
  tic
  % HACK at the moment
  warning( 'off', 'MATLAB:nearlySingularMatrix' );
  warning( 'off', 'MATLAB:singularMatrix' );
  warning( 'off', 'MATLAB:illConditionedMatrix' );
  x = fmincon( @(x)dynamicGraphEvolution_adimat_optDriver( x, opts ), x0, [], [], Aeq, beq, [], [], [], optOpts.fmincon );
  toc
end

% solution in matrix form
if ( params.optimizeOverY0 )
  [Y,H,Z] = getStateMatricesFromStateVector( x, [n p] );
else
  Y = params.Y0;
  [H,Z] = getStateMatricesFromStateVectorFixedY0( x, [n p] );
  xi = getStateVectorFromStateMatrices( params.Y0, H, Z );
  xf = computeDynamicGraphEvolution( xi, params.nt, [n p] );
  [YF_i,HF_i,ZF_i] = getStateMatricesFromStateVector( xf, [n p] );
end


