function L = createRandomLaplaceMatrix( n )

%[S,Y] = createRandomLaplaceMatrix( n )

% create a random symmetric matrix

p = n;
QR = randn(n,p);

% create the adjacency matrix
A = QR*QR';
A = abs(A);

for i=1:n
    A(i,i) = 0;
end

% now compute the graph laplacian

% degree matrix
D = zeros( n );
for i=1:n
    D(i,i) = sum( A(i,:) );
end

% now compute the Laplace matrix
L = D-A;

% PLEASE CHECK
%[U,S,~] = svd( L );

% now factor it
%QF = U(:,1:end-1)*sqrt( abs(S(1:end-1,1:end-1) ) );
%QF = QF+0.02*randn( size( QF ) );

%Y = 1/(n+1)*(ones(n)-n*eye(n))*QF;
%R = (eye(n)-1/n*ones(n));

%Y = R*QF;
%S = Y*Y';