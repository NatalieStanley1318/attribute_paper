%--------------------------------------------------------------------------
% config
%--------------------------------------------------------------------------
rng( 4323 );
n = 50; % nxn matrices
T = 20; % at T time points
noiseAmount = 0.05; % walk for t=0.05 into random direction
tmp = linspace( 0, 1, T );
time = zeros( T, 1 );
time(:,1) = tmp;

%--------------------------------------------------------------------------
% generate toy data
%--------------------------------------------------------------------------
A = createRandomLaplaceMatrix( n ); 
B = createRandomLaplaceMatrix( n );
Ahat = A + 1/n * ones( n, 1 )*ones( n, 1 )'; % extend by rank 1
Bhat = B + 1/n * ones( n, 1 )*ones( n, 1 )'; % extend by rank 1

%--------------------------------------------------------------------------
% work in log-Euclidean space to generate the toy data
%------------- -------------------------------------------------------------
tanVec = logm( Bhat ) - logm( Ahat );
Phat = cell( T, 1 );        % T PSD matrices along geodesic
noiseC = cell( T, 1 );      % T noisy PSD matrices along geodesic
for i=1:T
    % walk along geodesic
    Phat{i} = expm( logm( Ahat ) + time( i )*tanVec );
    % reduce by rank 1
    tmp = Phat{i} - 1/n*ones( n, 1 )*ones( n, 1 )';

    % project into Laplacians if required
    if ( isLaplace( tmp ) ~= 1 )
        fprintf('Not Laplacian ... trying to project!\n');
        tmp = projSDDZero( tmp );
        assert( isLaplace( tmp ) == 1 );
        % extend by rank 1
        tmp = tmp + 1/n*ones( n, 1 )*ones( n, 1 )';
        Phat{i} = tmp;
    end
    
    % DEBUG INFO:
    % Phat{i} ... Laplacians (extended by rank 1) along geodesic
    
    % create random Laplacian, extend by rank 1 and move towards this point
    % to create a noisy version of Phat{i}
    C = createRandomLaplaceMatrix( n );
    Chat = C + 1/n * ones( n, 1 )*ones( n, 1 )'; 
    tmpTanVec = logm( Phat{i} ) - logm( Chat );
    noiseChat = expm( logm( Phat{i} ) + noiseAmount * tmpTanVec );
    
    % reduce by rank 1
    noiseC{i} = noiseChat - 1/n*ones( n, 1 )*ones( n, 1 )';
    % project into Laplacians if required
    if ( isLaplace( noiseC{i} ) ~= 1 )
        fprintf('Not Laplacian ... trying to project!\n');
        noiseC{i} = projSDDZero( noiseC{i} );
        %assert( isLaplace( noiseC{i} ) == 1 );
    end
end

%--------------------------------------------------------------------------
% use Bathia's Riemannian framework on SPD matrices 
%------------- -------------------------------------------------------------
%G = sympositivedefinitefactory( n );
%tanVec = G.log( Ahat, Bhat );
%Phat = cell( T, 1 ); % T PSD matrices along geodesic
%noiseC = cell( T, 1 ); % T noisy PSD matrices along geodesic
%for i=1:T
%    Phat{i} = G.exp( Ahat, time(i)*tanVec );
%    C = createRandomLaplaceMatrix( n );
%    Chat = C + 1/n * ones( n, 1 )*ones( n, 1 )'; 
%    tmpTanVec = G.log( Phat{i}, Chat );
%    noiseChat = G.exp( Phat{i}, noiseAmount*tmpTanVec );
%    noiseC{i} = noiseChat - 1/n*ones( n, 1 )*ones( n, 1 )';
%end

%--------------------------------------------------------------------------
% perform regression in the log-Euclidean framework
%--------------------------------------------------------------------------
targetTime = T;
dataLogSpace = zeros( n*n, T );
for t=1:T
    % add rank 1
    tmp = noiseC{t} + 1/n * ones( n, 1 )*ones( n, 1 )';
    % work in log-Euclidean space
    tmp = logm(tmp);
    dataLogSpace(:,t) = tmp(:);
end

b = learnRR( dataLogSpace, 5, 0.1 ); % learn ridge regression
logSpaceP = reshape( predictRR( b, dataLogSpace, 5 ), n, n ); % predict
predictedPhat = expm( logSpaceP );
predictedP = predictedPhat - 1/n*ones( n, 1 )*ones( n, 1 )';
if ( isLaplace( predictedP ) ~= 1 )
    fprintf('Prediction not Laplacian ... trying to project!\n');
    predictedP = projSDDZero( predictedP );
    assert( isLaplace( predictedP ) == 1 );
end

%test simple vectorization of Laplacians + projecting back
dataEucSpace = zeros( n*n, T );
for t=1:T
    dataEucSpace(:,t) = noiseC{t}(:);
end
bEuc = learnRR( dataLogSpace, 5, 0.1 ); % learn ridge regression
eucSpaceP = reshape( predictRR( b, dataEucSpace, 5 ), n, n );
if ( isLaplace( eucSpaceP ) ~= 1 )
    fprintf('Prediction not Laplacian ... trying to project!\n');
    eucSpaceP = projSDDZero( eucSpaceP );
    %assert( isLaplace( eucSpaceP ) == 1 );
end

fprintf('*** in log-Euclidan framework ***\n');
disp( norm( B - predictedP, 'fro' ) );
%fprintf('w.r.t. noisy data: ||N(T)-P||_F=%0.3f\n', norm( noiseC{targetTime}-predictedP, 'fro' ) );
%fprintf('w.r.t. true data:  ||L(T)-P||_F=%0.3f\n', norm( Phat{targetTime} - predictedP, 'fro' ) );
fprintf('*** simple vec(L) in R^n ***\n');
disp( norm( B - eucSpaceP, 'fro' ) );
%fprintf('w.r.t. noisy data: ||N(T)-P||_F=%0.3f\n', norm( noiseC{targetTime}-eucSpaceP, 'fro' ) );
%fprintf('w.r.t. true data:  ||L(T)-P||_F=%0.3f\n', norm( Phat{targetTime} - eucSpaceP, 'fro' ) );









