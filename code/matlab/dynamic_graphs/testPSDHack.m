dbstop if error
symm = @(X) .5*(X+X');

%rng(1234);
cDir = pwd;
cd('~/Documents/MATLAB/manopt/');
importmanopt;
cd(cDir);

n=10;
evalAt = 0.7;
nTrials = 1000;


for i=1:nTrials
    A = symm(createRandomLaplaceMatrix( n )); % rank n-1
    B = symm(createRandomLaplaceMatrix( n )); % rank n-1
    assert(isLaplace(A)==1) 
    assert(isLaplace(B)==1) 
    
    Ahat = A + 1/n * ones( n, 1 )*ones( n, 1 )'; % increase rank by 1
    Bhat = B + 1/n * ones( n, 1 )*ones( n, 1 )'; % increase rank by 1

    %------------------------------------------
    % !!!!THIS FAILS FOR LOG-EUCLIDEAN SPACE!!!
    %------------------------------------------
    %T = logm(Bhat)-logm(Ahat);
    %Phat = expm(logm(Ahat)+evalAt*T);
    %assert(isLaplace(Phat - 1/n* ones(n,1)*ones(n,1)')==1);
    
    %---------------------------------------------
    % !!!!AND ALSO FAILS FOR AFFINE-INVARIANT!!!!!
    %---------------------------------------------
    G = sympositivedefinitefactory( n ); % PSD matrices
    T = symm(G.log( Ahat, Bhat )); % log-map (to get tangent A->B)
    Phat = symm(G.exp( Ahat, evalAt*T )); % point P somewhere on geodesic to Bhat
    P = Phat - 1/n* ones(n,1)*ones(n,1)';
    
    if isLaplace(P) ~= 1
        a = P;
        a = a - diag(diag(a));
        disp(a(a>0))
    end
end
fprintf('Passed %d trials!\n', nTrials); % otherwise assertions would have fired :)
