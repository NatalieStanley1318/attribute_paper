function Aeq = createAeqMatrix( Y0 )

n = size( Y0, 1 );
p = size( Y0, 2 );

Aeq = sparse( [], [], [], p*p, p*(p+1)/2 + n*p, 2*p*p*n );

for iI=1:p
  for iJ=1:p
    currentConstraintNr = sub2ind( [p p], iI, iJ );
    for iC=1:n
      currentZVariableNr = sub2ind( [n p], iC, iJ ) + p*(p+1)/2;
      Aeq( currentConstraintNr, currentZVariableNr ) = Y0( iC, iI );
    end
  end
end