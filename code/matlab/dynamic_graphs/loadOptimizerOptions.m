function opt = loadOptimizerOptions()

opt = [];

% optimizer options fminunc
options_fminunc = optimset('fminunc');
options_fminunc = optimset( options_fminunc, 'Display', 'iter' );
options_fminunc = optimset( options_fminunc, 'GradObj', 'on' );
options_fminunc = optimset( options_fminunc, 'Diagnostics', 'on' );
options_fminunc = optimset( options_fminunc, 'LargeScale', 'off' );
%options_fminunc = optimset( options_fminunc, 'DerivativeCheck', 'on' );
%options_fminunc = optimset( options_fminunc, 'Algorithm', 'interior-point' );

opt.fminunc = options_fminunc;

% optimizer options fmincon
options_fmincon = optimset('fmincon');
options_fmincon = optimset( options_fmincon, 'Display', 'iter' );
options_fmincon = optimset( options_fmincon, 'FunValCheck', 'off' );
%options_fmincon = optimset( options_fmincon, 'DiffMinChange', 0.001 );
%options_fmincon = optimset( options_fmincon, 'DiffMaxChange', 0.1 );
options_fmincon = optimset( options_fmincon, 'GradObj', 'on' );
options_fmincon = optimset( options_fmincon, 'algorithm', 'interior-point' );
options_fmincon = optimset( options_fmincon, 'Hessian','lbfgs' );
options_fmincon = optimset( options_fmincon, 'MaxFunEvals', 250 );

opt.fmincon = options_fmincon;

