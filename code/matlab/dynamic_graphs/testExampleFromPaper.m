clear all;
close all;

params.n = 2;
params.p = params.n-1;
params.sigma = 0.1;
params.nt = 50;
params.optimizeOverY0 = false;

t = 0:1/params.nt:1;

params.desired_S_TF = zeros( 2 );

Y0 = [1;0];
H0 = -1;
Z0 = [0;1];

x0 = getStateVectorFromStateMatrices( Y0, H0, Z0 );

[energyValue,debugOutput] = computeDynamicGraphEvolution( x0, params.nt, [params.n params.p] );

% now plot everything
[SE,YE,HE,ZE] = extractElements( debugOutput );

figure
title( 'Elements of S' );
hold on
plot( t, squeeze( SE(1,1,:) ), '-o' );
plot( t, squeeze( SE(1,2,:) ), '-x' );
plot( t, squeeze( SE(2,2,:) ), '-+' );

legend( 'S(1,1)', 'S(1,2)', 'S(2,2)', 'location', 'best' );

figure
title( 'Elements of Y' );
hold on
plot( t, squeeze( YE(1,1,:) ), '-o' );
plot( t, squeeze( YE(2,1,:) ), '-x' );

legend( 'Y(1,1)', 'Y(2,1)', 'location', 'best' );

figure
title( 'Elements of H' );
hold on
plot( t, squeeze( HE(1,1,:) ), '-o' );

legend( 'H(1,1)', 'location', 'best' );

figure
title( 'Elements of Z' );
hold on
plot( t, squeeze( ZE(1,1,:) ), '-o' );
plot( t, squeeze( ZE(2,1,:) ), '-x' );

legend( 'Z(1,1)', 'Z(2,1)', 'location', 'best' );