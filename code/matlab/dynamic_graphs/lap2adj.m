function A = lap2adj(L)
% LAP2ADJ returns the adjacency matrix A corresponding to a graph Laplacian
    A = -(L - diag(diag(L)));
end