function [f,g] = dynamicGraphEvolution_adimat_optDriver( x, optsAll )

params = optsAll.params;
sz = [params.n, params.p];

opts = optsAll.optsAdimat;

if ( nargout>1 )
  [g,f] = admDiffRev( @dynamicGraphEvolution_fixedRank, 1, x, params, opts );
else
  f = dynamicGraphEvolution_fixedRank( x, params );
end
  
