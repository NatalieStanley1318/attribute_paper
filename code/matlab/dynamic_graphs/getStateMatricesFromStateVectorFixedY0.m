function [H,Z] = getStateMatricesFromStateVectorFixedY0( x, sz )

n = sz(1);
p = sz(2);

% reconstruct the full H matrix from its upper triangular part
HTriag = x( 1:p*(p+1)/2 );
T = triu( ones( p ) );
H = zeros( p, p );
H( T==1 ) = HTriag;
HT = H';
H( T~=1 ) = HT( T==0 );

% get Z
Z = reshape( x( p*(p+1)/2+1:end ), sz );



