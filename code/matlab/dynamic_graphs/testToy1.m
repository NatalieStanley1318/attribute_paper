symm = @(X) .5*(X+X');

n=4;
a = 0;
% adjacency
A1 = [0 1 a a; ...
      1 0 a 2; ...
      a a 0 9; ...
      a 2 9 0];
A2 = [0 9 a a; ...
      9 0 a 2; ...
      a a 0 1; ...
      a 2 1 0];

assert( issymmetric( A1 ) );
assert( issymmetric( A2 ) );
  
% degree matrices
D1 = diag( sum( A1, 2 ) );
D2 = diag( sum( A2, 2 ) );

% Laplacians
L1 = D1 - A1; 
L2 = D2 - A2;

assert( isLaplace( L1 ) == 1);
assert( isLaplace( L2 ) == 1);

% rank-extended Laplacians
L1hat = L1 + 1/n * ones( n, 1 )*ones( n, 1 )';
L2hat = L2 + 1/n * ones( n, 1 )*ones( n, 1 )';

% From Bhatia book (see manopt)
G = sympositivedefinitefactory( n );
T = G.log(L1hat,L2hat);
extrapolatedP1 = G.exp( L1hat, 1.1*T ) - 1/n*ones( n, 1 )*ones( n, 1 )';
extrapolatedP1( abs( extrapolatedP1 ) <= 1e-9 ) = 0;
if isLaplace( extrapolatedP1 ) < 0
    Pproj1 = projSDDZero( extrapolatedP1 );

end

% Log-Euclidean
T = symm(logm( L2hat ) - logm( L1hat ));
Phat = symm(expm( logm( L1hat ) + 1.1*T ));
extrapolatedP2 = Phat - 1/n * ones( n, 1 )*ones( n, 1 )';
if isLaplace( extrapolatedP2 ) < 0
    Pproj2 = projSDDZero( extrapolatedP2 );
end






