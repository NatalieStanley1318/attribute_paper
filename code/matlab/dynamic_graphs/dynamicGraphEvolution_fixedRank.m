function energyValue = dynamicGraphEvolution_fixedRank( x, params )

Y = [];
H = [];
Z = [];
Y_TF = [];
H_TF = [];
Z_TF = [];

szn = params.n;
szp = params.p;

% x is a column vector which contains all the unknowns
sz = [szn szp];

if ( params.optimizeOverY0 )
  [Y,H,Z] = getStateMatricesFromStateVector( x, sz );
else
  Y = params.Y0;
  [H,Z] = getStateMatricesFromStateVectorFixedY0( x, sz );
end

% energy will be composed of energy for the initial value as well as a final matching energy

energyValue = 0;

% initial value energy
YP = Y*H + (Z-Y*inv(Y'*Y)*Y'*Z );
SP = YP*Y' + Y*YP';

energyValue = energyValue + sum( diag ( SP'*SP ) );

% now integrate up to unit time in nt steps

% this now includes Y even if it is not optimized over
xc = getStateVectorFromStateMatrices( Y, H, Z );

xc = computeDynamicGraphEvolution( xc, params.nt, sz );
[Y_TF,H_TF,Z_TF] = getStateMatricesFromStateVector( xc, sz );

S_TF = Y_TF*Y_TF'; % S matrix after final integration
% now compare this to the desired final matrix

energyValue = energyValue + 1/(params.sigma^2)*sum( sum( ( S_TF - params.desired_S_TF ).^2 ) );

end


