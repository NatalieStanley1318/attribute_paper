function [Y,H,Z] = getStateMatricesFromStateVector( x, sz )

n = sz(1);
p = sz(2);

% get Y
Y = reshape( x( 1:prod( sz ) ), sz );

% reconstruct the full H matrix from its upper triangular part
HTriag = x( prod(sz)+1:prod(sz)+p*(p+1)/2 );
T = triu( ones( p ) );
H = zeros( p, p );
H( T==1 ) = HTriag;
HT = H';
H( T~=1 ) = HT( T==0 );

% get Z
Z = reshape( x( prod(sz)+p*(p+1)/2+1:end ), sz );



