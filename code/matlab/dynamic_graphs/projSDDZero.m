function Y = projSDDZero( X0 )
% projSDDZero projects a symmetric matrix onto the closest Laplacian in the
% sense of the Frobenius norm.
%
% NOTES: needs CVX
%
% Author: Roland Kwitt, Marc Niethammer, 2015

    [n,m] = size( X0 );
    assert( n == m, 'X0 not square!' );

    % optimize
    cvx_begin sdp quiet
        variable X(n,n) symmetric
        minimize ( norm( X-X0,'fro' ) )
        % (weak) diagonal dominance
        diag( X ) - sum( abs( X-diag( diag( X ) ) ), 2 ) >= 0; %#ok<VUNUS>
        % positive diagonal (via eps)
        diag( X ) >= eps; %#ok<VUNUS>
        % row/column sum of 0
        X*ones(n,1) == 0;  %#ok<EQEFF>
    cvx_end
    Y = X;
end