function param = learnRR( A, m, gamma )
% learnRR Sliding-window based ridge regression.
%
% param = learnRR( A, m, gamma )
%
%   Input:
%   ------
%   M       - Data matrix of size (dim x #signals)
%   m       - Sliding window length
%   gamma   - see MATLAB's 'ridge' function
%
%   Output:
%   -------
%   param   - (m+1)-dimensional parameter vector that
%             can be used with 'predictRR'
%
% Author(s): Roland Kwitt, Marc Niethammer
% Date: April, 2015

    T = size( A, 2 ); % T times
    n = size( A, 1 ); % n-dim. signal
    
    W = cell( T-m+1, 1 ); % cell array of sliding window indices
    for t=1:T-m+1
        W{t} = t:t+m-1;
    end
    
    X = zeros( n*( length( W )-1 ), m ); % trn
    y = zeros( n*( length( W )-1 ), 1 ); % tst
    
    for i=1:length( W )-1
        idx = W{i};
        assert( length( idx )==m );
        X( ( i-1 )*n+1:i*n, : ) = A( :, idx );
        y( ( i-1 )*n+1:i*n) = A( :, idx( end )+1 );
    end
    param = ridge( y, X, gamma, 0 );
end