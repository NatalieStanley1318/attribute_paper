function x = getStateVectorFromStateMatrices( Y, H, Z )

x = [Y(:); extractUpperTriangularPartAsVector( H ); Z(:)];

function x = extractUpperTriangularPartAsVector( H )

x= H( triu( ones( size( H ) ) )==1 );