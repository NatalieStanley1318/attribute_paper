function [Y0I,H0I,Z0I] = computeInitialization( Y0, YF )

n = size( Y0, 1 );
p = size( Y0, 2 );

Y0I = Y0;

SF = YF*YF';
H0I = 0.5*( inv(Y0'*Y0)*Y0'*( ( SF' + SF )/2 )*Y0*inv( Y0'*Y0 ) - eye( p ) );

Z0I = zeros( n, p );