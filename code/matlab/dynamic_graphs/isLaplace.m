function [retVal] = isLaplace( L )
% isLaplace checks if a matrix is a valid Laplace matrix
%
% Author: Roland Kwitt, Marc Niethammer, 2015

    retVal = 0;
    [n,m] = size( L );
    assert( n == m, 'Matrix not square!' );
    
    [~,eVal] = eig( L );
    chk = find( abs( diag( eVal ) ) <1e-9 ); 
    if length( chk ) ~= 1
        fprintf('#zero eigenvalues != 1\n');
        retVal = retVal - 1;
    end
    
    if ( any( diag( L ) <= 0 ) )
        fprintf('diagonal not positive!\n');
        retVal = retVal - 1;
    end
        
    S = sum(sum(-(L-diag(diag(L)))>=0));
    if S ~= n^2
        fprintf('negativity of off-diagonals violated!\n');
        retVal = retVal - 1;
    end
    
    if ( any( abs( sum( L, 2 ) ) >= 1e6 ) )
        fprintf('column/row sum != 0\n');
        retVal = retVal - 1;
    end
    
    if retVal ~= 0
        fprintf( '%d conditions violated!\n', abs( retVal ) );
        return;
    else
        retVal = 1;
        return;
    end
end